-- Volcando datos para la tabla California.cliente: ~1 rows (aproximadamente)
/*!40000 ALTER TABLE `cliente` DISABLE KEYS */;
INSERT IGNORE INTO `cliente` (`id_cliente`, `Nombre`, `Email`, `Password`, `Telefono`, `RFC`) VALUES
	(1, 'Erik', 'borredrilo@gmail.com', "hola", '123456', 'GOHE2809985P4'),
	(2, 'Juan', 'JuanUAABD@uaa.mx', "hola", '12345678', 'JUAH281443'),
	(3, 'Carlos', 'carlosdolor@gmail.com', "hola", '123456789', 'CARL478531');
/*!40000 ALTER TABLE `cliente` ENABLE KEYS */;


-- Volcando datos para la tabla California.compra: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `compra` DISABLE KEYS */;
INSERT IGNORE INTO `compra` (`id_compra`, `id_proveedor`, `id_empleado`, `Fecha`, `Surtido`, `Total`) VALUES
	(1, 1, 2, '2019-11-28', 1, 10000),
	(2, 2, 2, '2019-11-28', 1, 5000),
	(3, 1, 1, '2019-11-28', 1, 7000),
	(4, 1, 2, '2019-11-28', 0, 2000),
	(5, 2, 1, '2019-11-28', 0, 6000);
/*!40000 ALTER TABLE `compra` ENABLE KEYS */;

-- Volcando datos para la tabla California.compraproducto: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `compraproducto` DISABLE KEYS */;
INSERT IGNORE INTO `compraproducto` (`id_producto`, `id_compra`, `Cantidad`, `Precio`) VALUES
	(5, 1, 100, 6000),
	(2, 1, 50, 4000),
	(4, 2, 110, 5000),
	(3, 4, 200, 2000),
	(1, 5, 250, 3540),
	(3, 5, 50, 2460),
	(4, 3, 500, 7000);
/*!40000 ALTER TABLE `compraproducto` ENABLE KEYS */;

-- Volcando datos para la tabla California.empleado: ~1 rows (aproximadamente)
/*!40000 ALTER TABLE `empleado` DISABLE KEYS */;
INSERT IGNORE INTO `empleado` (`id_empleado`, `Nombre`, `Puesto`, `id_Jefe`, `Telefono`, `Email`, `Username`, `Password`) VALUES
	(1, 'Kristian', 'Manager JR', 1, '1234567', 'kristian.alba@gmail.com', 'kristian', _binary 0x686F6C61),
	(2, 'Alejandra', 'Vendedor', 1, '1234567', 'alejandra.ponce@gmail.com', 'alejandra', _binary 0x686F6C61);
/*!40000 ALTER TABLE `empleado` ENABLE KEYS */;


-- Volcando datos para la tabla California.fruteriainfo: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `fruteriainfo` DISABLE KEYS */;
INSERT IGNORE INTO `fruteriainfo` (`Nombre`, `Direccion`, `RFC`) VALUES
	('Fruteria California', 'Arnulfo M. Valdéz 26, Centro Comercial Agropecuario, 20135 Aguascalientes, Ags.', 'CALI784596');
/*!40000 ALTER TABLE `fruteriainfo` ENABLE KEYS */;

-- Volcando datos para la tabla California.pedido: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `pedido` DISABLE KEYS */;
INSERT IGNORE INTO `pedido` (`id_pedido`, `id_cliente`, `id_empleado`, `Fecha`, `Estado`, `Surtido`, `Total`) VALUES
	(1, 1, 1, '2019-11-28', 'Creado', b'0', 500),
	(2, 3, 2, '2019-11-28', 'Creado', b'0', 2000),
	(3, 2, 1, '2019-11-28', 'Pagado', b'0', 200),
	(4, 1, 2, '2019-11-28', 'Pagado', b'1', 500);
/*!40000 ALTER TABLE `pedido` ENABLE KEYS */;

-- Volcando datos para la tabla California.pedidoproducto: ~1 rows (aproximadamente)
/*!40000 ALTER TABLE `pedidoproducto` DISABLE KEYS */;
INSERT IGNORE INTO `pedidoproducto` (`id_producto`, `id_pedido`, `Cantidad`, `Precio`) VALUES
	(1, 1, 4, 9),
	(3, 3, 6, 500),
	(4, 2, 3, 2000),
	(2, 4, 10, 500);
/*!40000 ALTER TABLE `pedidoproducto` ENABLE KEYS */;



-- Volcando datos para la tabla California.producto: ~1 rows (aproximadamente)
/*!40000 ALTER TABLE `producto` DISABLE KEYS */;
INSERT IGNORE INTO `producto` (`id_producto`, `Descripcion`, `CostoCompra`, `CostoVenta`, `id_productoCategoria`, `Existencia`, `PuntoReorden`, `UnidadMedida`, `Nombre`) VALUES
	(1, 'Platano', 4, 9, 1, 1000, 20, 'GRANEL', 'Platano'),
	(2, 'Manzana', 3, 8, 1, 2000, 10, 'GRANEL', 'Manzana'),
	(3, 'Uva', 7, 15, 1, 1500, 50, 'GRANEL', 'Uva'),
	(4, 'Calabaza', 4, 8, 2, 900, 10, 'GRANEL', 'Calabaza'),
	(5, 'Fresa', 6, 12, 1, 1000, 25, 'PIEZA/UNIDAD', 'Fresa'),
	(6, 'Zanahoria', 2, 8, 2, 1500, 55, 'GRANEL', 'Zanahoria');
/*!40000 ALTER TABLE `producto` ENABLE KEYS */;


-- Volcando datos para la tabla California.productocategoria: ~1 rows (aproximadamente)
/*!40000 ALTER TABLE `productocategoria` DISABLE KEYS */;
INSERT IGNORE INTO `productocategoria` (`id_productoCategoria`, `Descripcion`, `Nombre`) VALUES
	(1, 'Frutas', 'Fruta'),
	(2, 'Verduras', 'Verdura');
/*!40000 ALTER TABLE `productocategoria` ENABLE KEYS */;


-- Volcando datos para la tabla California.proveedor: ~2 rows (aproximadamente)
/*!40000 ALTER TABLE `proveedor` DISABLE KEYS */;
INSERT IGNORE INTO `proveedor` (`id_proveedor`, `Nombre`, `Direccion`, `Telefono`, `Whatsapp`, `RFC`, `Email`) VALUES
	(1, 'Antonio', 'Francisco Villa', '4961290089', '4961290089', 'CIAA960923', 'crackboy999@gmail.com'),
	(2, 'Monica', 'Pabellon ', '1237485961', '7418529632', 'VALD741263', 'monicauaa@gmail.com');
/*!40000 ALTER TABLE `proveedor` ENABLE KEYS */;

-- Volcando datos para la tabla California.proveedorproductos: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `proveedorproductos` DISABLE KEYS */;
INSERT IGNORE INTO `proveedorproductos` (`id_proveedor`, `id_producto`) VALUES
	(1, 1),
	(1, 4),
	(1, 5),
	(1, 6),
	(2, 3),
	(2, 2),
	(2, 4),
	(2, 1),
	(1, 2),
	(1, 3),
	(2, 5),
	(2, 6);
/*!40000 ALTER TABLE `proveedorproductos` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;