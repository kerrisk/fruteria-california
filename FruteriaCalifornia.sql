CREATE DATABASE IF NOT EXISTS Fruteria;
USE Fruteria;
ALTER TABLE productosCompras DROP FOREIGN KEY FKproductosC611517;
ALTER TABLE productosCompras DROP FOREIGN KEY FKproductosC937176;
ALTER TABLE Compras DROP FOREIGN KEY FKCompras889161;
ALTER TABLE Compras DROP FOREIGN KEY FKCompras719668;
ALTER TABLE Ventas DROP FOREIGN KEY FKVentas687098;
ALTER TABLE productosVentas DROP FOREIGN KEY FKproductosV382289;
ALTER TABLE productosVentas DROP FOREIGN KEY FKproductosV729308;
DROP TABLE IF EXISTS Administradores;
DROP TABLE IF EXISTS Productos;
DROP TABLE IF EXISTS Clientes;
DROP TABLE IF EXISTS Compras;
DROP TABLE IF EXISTS productosCompras;
DROP TABLE IF EXISTS Proveedor;
DROP TABLE IF EXISTS Ventas;
DROP TABLE IF EXISTS productosVentas;
CREATE TABLE Administradores (
  username varchar(100), 
  password varchar(100), 
  id       int(10) NOT NULL AUTO_INCREMENT, 
  Nombre   varchar(100), 
  PRIMARY KEY (id));
CREATE TABLE Productos (
  id           int(10) NOT NULL AUTO_INCREMENT, 
  Nombre       varchar(100), 
  precioCompra int(10), 
  precioVenta  int(10), 
  Cantidad     int(10), 
  PRIMARY KEY (id));
CREATE TABLE Clientes (
  id       int(10) NOT NULL AUTO_INCREMENT, 
  Nombre   varchar(100), 
  RFC      varchar(30), 
  `Column` int(10), 
  PRIMARY KEY (id));
CREATE TABLE Compras (
  id                int(10) NOT NULL AUTO_INCREMENT, 
  CostoTotal        int(10), 
  Fecha             date, 
  Proveedorid       int(10) NOT NULL, 
  Administradoresid int(10) NOT NULL, 
  PRIMARY KEY (id));
CREATE TABLE productosCompras (
  Cantidad     int(10), 
  precioVenta  int(10), 
  precioCompra int(10), 
  Productosid  int(10) NOT NULL, 
  Comprasid    int(10) NOT NULL);
CREATE TABLE Proveedor (
  id       int(10) NOT NULL AUTO_INCREMENT, 
  Nombre   varchar(100), 
  telefono int(10), 
  RFC      varchar(30), 
  PRIMARY KEY (id));
CREATE TABLE Ventas (
  id         int(10) NOT NULL AUTO_INCREMENT, 
  CostoVenta int(10), 
  Fecha      date, 
  Clientesid int(10) NOT NULL, 
  PRIMARY KEY (id));
CREATE TABLE productosVentas (
  Cantidad     int(10), 
  precioVenta  int(10), 
  precioCompra int(10), 
  Ventasid     int(10) NOT NULL, 
  Productosid  int(10) NOT NULL);
ALTER TABLE productosCompras ADD CONSTRAINT FKproductosC611517 FOREIGN KEY (Productosid) REFERENCES Productos (id);
ALTER TABLE productosCompras ADD CONSTRAINT FKproductosC937176 FOREIGN KEY (Comprasid) REFERENCES Compras (id);
ALTER TABLE Compras ADD CONSTRAINT FKCompras889161 FOREIGN KEY (Proveedorid) REFERENCES Proveedor (id);
ALTER TABLE Compras ADD CONSTRAINT FKCompras719668 FOREIGN KEY (Administradoresid) REFERENCES Administradores (id);
ALTER TABLE Ventas ADD CONSTRAINT FKVentas687098 FOREIGN KEY (Clientesid) REFERENCES Clientes (id);
ALTER TABLE productosVentas ADD CONSTRAINT FKproductosV382289 FOREIGN KEY (Ventasid) REFERENCES Ventas (id);
ALTER TABLE productosVentas ADD CONSTRAINT FKproductosV729308 FOREIGN KEY (Productosid) REFERENCES Productos (id);
