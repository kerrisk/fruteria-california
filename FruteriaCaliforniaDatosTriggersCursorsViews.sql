-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versión del servidor:         10.4.10-MariaDB - mariadb.org binary distribution
-- SO del servidor:              Win64
-- HeidiSQL Versión:             10.2.0.5599
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Volcando estructura de base de datos para California
DROP DATABASE IF EXISTS `California`;
CREATE DATABASE IF NOT EXISTS `California` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `California`;

-- Volcando estructura para tabla California.cliente
DROP TABLE IF EXISTS `cliente`;
CREATE TABLE IF NOT EXISTS `cliente` (
  `id_cliente` int(10) NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(255) DEFAULT NULL,
  `Email` varchar(255) DEFAULT NULL,
  `Password` varchar(255) DEFAULT NULL,
  `Telefono` varchar(10) DEFAULT NULL,
  `RFC` varchar(13) DEFAULT NULL,
  `ruta` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_cliente`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;



-- Volcando estructura para tabla California.clienteimagen
/* DROP TABLE IF EXISTS `clienteimagen`;
CREATE TABLE IF NOT EXISTS `clienteimagen` (
  `id_imagenCliente` int(10) NOT NULL AUTO_INCREMENT,
  `id_cliente` int(10) NOT NULL,
  `ruta` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_imagenCliente`),
  KEY `fk_clienteImagen_cliente` (`id_cliente`),
  CONSTRAINT `fk_clienteImagen_cliente` FOREIGN KEY (`id_cliente`) REFERENCES `cliente` (`id_cliente`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1; */

-- Volcando datos para la tabla California.clienteimagen: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `clienteimagen` DISABLE KEYS */;
/*!40000 ALTER TABLE `clienteimagen` ENABLE KEYS */;

-- Volcando estructura para tabla California.compra
DROP TABLE IF EXISTS `compra`;
CREATE TABLE IF NOT EXISTS `compra` (
  `id_compra` int(10) NOT NULL AUTO_INCREMENT,
  `id_proveedor` int(10) NOT NULL,
  `id_empleado` int(10) NOT NULL,
  `Fecha` date DEFAULT NULL,
  `Surtido` tinyint(1) DEFAULT NULL,
  `Total` float DEFAULT NULL,
  PRIMARY KEY (`id_compra`),
  KEY `fk_compra_empleado` (`id_empleado`),
  KEY `fk_compra_proveedor` (`id_proveedor`),
  CONSTRAINT `fk_compra_empleado` FOREIGN KEY (`id_empleado`) REFERENCES `empleado` (`id_empleado`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_compra_proveedor` FOREIGN KEY (`id_proveedor`) REFERENCES `proveedor` (`id_proveedor`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- Volcando estructura para tabla California.compraproducto
DROP TABLE IF EXISTS `compraproducto`;
CREATE TABLE IF NOT EXISTS `compraproducto` (
  `id_producto` int(10) NOT NULL,
  `id_compra` int(10) DEFAULT NULL,
  `Cantidad` float DEFAULT NULL,
  `Precio` float DEFAULT NULL,
  KEY `fk_compraProd_Producto` (`id_producto`),
  KEY `fk_compraProd_Compra` (`id_compra`),
  CONSTRAINT `fk_compraProd_Compra` FOREIGN KEY (`id_compra`) REFERENCES `compra` (`id_compra`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_compraProd_Producto` FOREIGN KEY (`id_producto`) REFERENCES `producto` (`id_producto`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



-- Volcando estructura para tabla California.empleado
DROP TABLE IF EXISTS `empleado`;
CREATE TABLE IF NOT EXISTS `empleado` (
  `id_empleado` int(10) NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(255) DEFAULT NULL,
  `Puesto` varchar(255) DEFAULT NULL,
  `id_Jefe` int(10) NOT NULL,
  `Telefono` varchar(10) DEFAULT NULL,
  `Email` varchar(255) DEFAULT NULL,
  `Username` varchar(255) DEFAULT NULL,
  `Password` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_empleado`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;


-- Volcando estructura para tabla California.fruteriainfo
DROP TABLE IF EXISTS `fruteriainfo`;
CREATE TABLE IF NOT EXISTS `fruteriainfo` (
  `Nombre` varchar(255) DEFAULT NULL,
  `Direccion` varchar(255) DEFAULT NULL,
  `RFC` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



-- Volcando estructura para tabla California.pedido
DROP TABLE IF EXISTS `pedido`;
CREATE TABLE IF NOT EXISTS `pedido` (
  `id_pedido` int(10) NOT NULL AUTO_INCREMENT,
  `id_cliente` int(10) NOT NULL,
  `id_empleado` int(10) NOT NULL,
  `Fecha` date DEFAULT NULL,
  `Estado` varchar(255) DEFAULT NULL,
  `Surtido` bit(1) DEFAULT NULL,
  `Total` float DEFAULT NULL,
  PRIMARY KEY (`id_pedido`),
  KEY `fk_pedido_cliente` (`id_cliente`),
  KEY `fk_pedido_empleado` (`id_empleado`),
  CONSTRAINT `fk_pedido_cliente` FOREIGN KEY (`id_cliente`) REFERENCES `cliente` (`id_cliente`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_pedido_empleado` FOREIGN KEY (`id_empleado`) REFERENCES `empleado` (`id_empleado`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;



-- Volcando estructura para tabla California.pedidoproducto
DROP TABLE IF EXISTS `pedidoproducto`;
CREATE TABLE IF NOT EXISTS `pedidoproducto` (
  `id_producto` int(10) NOT NULL,
  `id_pedido` int(10) DEFAULT NULL,
  `Cantidad` float DEFAULT NULL,
  `Precio` float DEFAULT NULL,
  KEY `fk_pediProd_Producto` (`id_producto`),
  KEY `fk_pediProd_Pedido` (`id_pedido`),
  CONSTRAINT `fk_pediProd_Pedido` FOREIGN KEY (`id_pedido`) REFERENCES `pedido` (`id_pedido`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_pediProd_Producto` FOREIGN KEY (`id_producto`) REFERENCES `producto` (`id_producto`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


-- Volcando estructura para tabla California.producto
DROP TABLE IF EXISTS `producto`;
CREATE TABLE IF NOT EXISTS `producto` (
  `id_producto` int(10) NOT NULL AUTO_INCREMENT,
  `Descripcion` varchar(255) DEFAULT NULL,
  `CostoCompra` float DEFAULT NULL,
  `CostoVenta` float DEFAULT NULL,
  `id_productoCategoria` int(10) DEFAULT NULL,
  `Existencia` float DEFAULT NULL,
  `PuntoReorden` float DEFAULT NULL,
  `UnidadMedida` varchar(255) DEFAULT NULL,
  `Nombre` varchar(255) DEFAULT NULL,
  `ruta` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_producto`),
  KEY `fk_producto_prodCategoria` (`id_productoCategoria`),
  CONSTRAINT `fk_producto_prodCategoria` FOREIGN KEY (`id_productoCategoria`) REFERENCES `productocategoria` (`id_productoCategoria`) ON UPDATE CASCADE,
  CONSTRAINT `ck_unidadMedida` CHECK (`UnidadMedida` in ('Granel','Pieza/Unidad'))
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;


-- Volcando estructura para tabla California.productocategoria
DROP TABLE IF EXISTS `productocategoria`;
CREATE TABLE IF NOT EXISTS `productocategoria` (
  `id_productoCategoria` int(10) NOT NULL,
  `Descripcion` varchar(255) DEFAULT NULL,
  `Nombre` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_productoCategoria`),
  KEY `id_productoCategoria` (`id_productoCategoria`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


-- Volcando estructura para tabla California.productoimagen
/* DROP TABLE IF EXISTS `productoimagen`;
CREATE TABLE IF NOT EXISTS `productoimagen` (
  `id_productoImagen` int(10) NOT NULL,
  `id_producto` int(10) NOT NULL,
  `ruta` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_productoImagen`),
  KEY `fk_ProdImagen_Producto` (`id_producto`),
  CONSTRAINT `fk_ProdImagen_Producto` FOREIGN KEY (`id_producto`) REFERENCES `producto` (`id_producto`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1; */

-- Volcando datos para la tabla California.productoimagen: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `productoimagen` DISABLE KEYS */;
/*!40000 ALTER TABLE `productoimagen` ENABLE KEYS */;

-- Volcando estructura para tabla California.proveedor
DROP TABLE IF EXISTS `proveedor`;
CREATE TABLE IF NOT EXISTS `proveedor` (
  `id_proveedor` int(10) NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(255) DEFAULT NULL,
  `Direccion` varchar(255) DEFAULT NULL,
  `Telefono` varchar(10) DEFAULT NULL,
  `Whatsapp` varchar(10) DEFAULT NULL,
  `RFC` varchar(13) DEFAULT NULL,
  `Email` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_proveedor`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Volcando estructura para tabla California.proveedorproductos
DROP TABLE IF EXISTS `proveedorproductos`;
CREATE TABLE IF NOT EXISTS `proveedorproductos` (
  `id_proveedor` int(10) NOT NULL,
  `id_producto` int(10) NOT NULL,
  KEY `fk_proveProduct_Proveedor` (`id_proveedor`),
  KEY `fk_proveProduct_Producto` (`id_producto`),
  CONSTRAINT `fk_proveProduct_Producto` FOREIGN KEY (`id_producto`) REFERENCES `producto` (`id_producto`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_proveProduct_Proveedor` FOREIGN KEY (`id_proveedor`) REFERENCES `proveedor` (`id_proveedor`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



/*--------------------------VIEWS----------------------*/
  -- • Mostrar todos los clientes activos (con una compra en el ultimo mes)
      CREATE OR REPLACE VIEW ClientesActivos AS (
          SELECT DISTINCT * FROM cliente WHERE cliente.id_Cliente in (
              SELECT DISTINCT id_cliente FROM pedido WHERE DATEDIFF(SYSDATE(), Fecha) < 31 
              )
      );
  -- • Mostrar todos los productos de la categoria frutas
      CREATE OR REPLACE VIEW Frutas AS (
          SELECT * FROM producto where id_productoCategoria in (
              SELECT id_productoCategoria FROM productocategoria WHERE Nombre = "Fruta"
          )
      );
  -- • Mostrar todos los productos de la categoria verduras
      CREATE OR REPLACE VIEW verduras AS (
          SELECT * FROM producto where id_productoCategoria in (
              SELECT id_productoCategoria FROM productocategoria WHERE Nombre = "Verdura"
          )
      );
  -- • Mostrar las compras que realizo un empleado filtrando con su nombre 
      CREATE OR REPLACE VIEW ComprasAlejandra AS (
          SELECT * FROM compra WHERE id_empleado in (
              SELECT id_empleado FROM empleado WHERE nombre = "Alejandra"
          )
      );
  -- • Mostrar los pedidos que realizo un empleado filtrando con su nombre  
      CREATE OR REPLACE VIEW PedidosAlejandra AS (
          SELECT * FROM pedido WHERE id_empleado in (
              SELECT id_empleado FROM empleado WHERE nombre = "Alejandra"
          )
      );
      -- VIEWS
    -- Mostrar todos los clientes activos (con una compra en el ultimo mes)
        CREATE OR REPLACE VIEW ClientesActivos AS (
            SELECT DISTINCT * FROM cliente WHERE cliente.id_Cliente in (
                SELECT DISTINCT id_cliente FROM pedido WHERE DATEDIFF(SYSDATE(), Fecha) < 31 
                )
        );
     -- Mostrar todos los productos de la categoria frutas
        CREATE OR REPLACE VIEW Frutas AS (
            SELECT * FROM producto where id_productoCategoria in (
                SELECT id_productoCategoria FROM productocategoria WHERE Nombre = "Fruta"
            )
        );
     -- Mostrar todos los productos de la categoria verduras
        CREATE OR REPLACE VIEW verduras AS (
            SELECT * FROM producto where id_productoCategoria in (
                SELECT id_productoCategoria FROM productocategoria WHERE Nombre = "Verdura"
            )
        );
     -- Mostrar las compras que realizo un empleado filtrando con su nombre 
        CREATE OR REPLACE VIEW ComprasAlejandra AS (
            SELECT * FROM compra WHERE id_empleado in (
                SELECT id_empleado FROM empleado WHERE nombre = "Alejandra"
            )
        );
     -- Mostrar los pedidos que realizo un empleado filtrando con su nombre  
         CREATE OR REPLACE VIEW PedidosAlejandra AS (
            SELECT * FROM pedido WHERE id_empleado in (
                SELECT id_empleado FROM empleado WHERE nombre = "Alejandra"
            )
        );
--*TODO
    -- Cursor en la tabla producto que calula el precio estimado de todo lo que 
    -- se tiente en la fruteria (multiplicando Existencia * CostoVenta) y lo almacena en t1
    DROP PROCEDURE IF EXISTS p1;
    DROP TABLE IF EXISTS t1;
    CREATE TABLE t1 (existenciaxprecio float);
    DELIMITER //
    CREATE PROCEDURE p1()
    BEGIN
    DECLARE done INT DEFAULT FALSE;
    DECLARE Vexistencia float;
    DECLARE Vcostoventa float;
    DECLARE cur CURSOR FOR SELECT existencia, costoventa FROM producto;
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done=TRUE;
    OPEN cur;
    read_loop: LOOP
        FETCH cur INTO Vexistencia , Vcostoventa;
        IF done THEN
        LEAVE read_loop;
        END IF;
        INSERT INTO t1 VALUES (Vexistencia * Vcostoventa);
    END LOOP;
    CLOSE cur; 
    END;
    //
    DELIMITER ;

    -- Cursor en la tabla t1 que suma todo y lo almacena en t2
    DROP PROCEDURE IF EXISTS p2;
    DROP TABLE IF EXISTS t2;
    CREATE TABLE t2 (existenciaxprecioSUM float);
    DELIMITER //
    CREATE PROCEDURE p2()
    BEGIN
    DECLARE done2 INT DEFAULT FALSE;
    DECLARE Vtemp float;
    DECLARE VSUM float;
    DECLARE cur2 CURSOR FOR SELECT existenciaxprecio FROM t1;
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done2=TRUE;
    SET Vtemp=0;
    OPEN cur2;
    read_loop: LOOP
        FETCH cur2 INTO Vtemp;
        IF done THEN
        LEAVE read_loop;
        END IF;
        SET VSUM+=Vtemp;
    END LOOP;
    INSERT INTO t2 VALUES (VSUM);
    CLOSE cur2; 
    END;
    //
    DELIMITER ;

  -- Cursor en la tabla producto que la difrencia de existencia con  el punto de reoorden
  -- de los productos.
    DROP PROCEDURE IF EXISTS p3;
    DROP TABLE IF EXISTS t3;
    CREATE TABLE t3 (existenciampreorden float);
    DELIMITER //
    CREATE PROCEDURE p3()
    BEGIN
    DECLARE done3 INT DEFAULT FALSE;
    DECLARE Vexistencia float;
    DECLARE Vpuntoreorden float;
    DECLARE cur CURSOR FOR SELECT existencia, puntoreorden FROM producto;
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done3=TRUE;
    OPEN cur;
    read_loop3: LOOP
        FETCH cur INTO Vexistencia , Vpuntoreorden;
        IF done3 THEN
        LEAVE read_loop3;
        END IF;
        INSERT INTO t3 VALUES (Vexistencia - Vpuntoreorden);
    END LOOP;
    CLOSE cur; 
    END;
    //
    DELIMITER ;

    -- Cursor en la tabla pedidoproducto que calula el importe total de cada pedidioproducto (multiplicando cantidad por precio) lo almacena en t4
    DROP PROCEDURE IF EXISTS p4;
    DROP TABLE IF EXISTS t4;
    CREATE TABLE t4 (importeTotalPedido float);
    DELIMITER //
    CREATE PROCEDURE p4()
    BEGIN
    DECLARE done INT DEFAULT FALSE;
    DECLARE Vcant float;
    DECLARE Vprecio float;
    DECLARE cur CURSOR FOR SELECT cantidad, precio FROM pedidoproducto;
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done=TRUE;
    OPEN cur;
    read_loop: LOOP
        FETCH cur INTO Vcant , Vprecio;
        IF done THEN
        LEAVE read_loop;
        END IF;
        INSERT INTO t4 VALUES (Vcant * Vprecio);
    END LOOP;
    CLOSE cur; 
    END;
    //
    DELIMITER ;

        -- Cursor en la tabla compraproducto que calula el importe total de cada producto 
    DROP PROCEDURE IF EXISTS p5;
    DROP TABLE IF EXISTS t5;
    CREATE TABLE t5 (importeTotalPedido float);
    DELIMITER //
    CREATE PROCEDURE p5()
    BEGIN
    DECLARE done INT DEFAULT FALSE;
    DECLARE Vcant float;
    DECLARE Vprecio float;
    DECLARE cur CURSOR FOR SELECT cantidad, precio FROM compraproducto;
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done=TRUE;
    OPEN cur;
    read_loop: LOOP
        FETCH cur INTO Vcant , Vprecio;
        IF done THEN
        LEAVE read_loop;
        END IF;
        INSERT INTO t5 VALUES (Vcant * Vprecio);
    END LOOP;
    CLOSE cur; 
    END;
    //
    DELIMITER ;
    -- Trigger para chequear si el valor esta dentro del dominio pedido.estado
    delimiter $
    create trigger pedido_estado_validar before insert on pedido
    for each row
    begin
        if new.estado not in ('Creado','Pagado')
        then
            signal sqlstate '45000' set message_text = 'mal estado de pedido';
        end if;
    end$
    delimiter ;

    -- Trigger para chequear si el valor esta dentro del dominio producto.unidadmedida
    delimiter $
    create trigger producto_estado_validar before insert on producto
    for each row
    begin
        if new.unidadmedida not in ('Granel','Pieza/Unidad')
        then
            signal sqlstate '45000' set message_text = 'mal unidad/medida de producto';
        end if;
    end$
    delimiter ;

    -- trigger para mantener la cuenta de productos dados de alta en la DB
    CREATE TABLE producto_count (productos int);
    INSERT INTO producto_count (productos) VALUES(0);
    CREATE TRIGGER increment_producto
    AFTER INSERT ON producto 
    FOR EACH ROW 
    UPDATE producto_count SET producto_count.productos = producto_count.productos+1;

    -- trigger para mantener la cuenta de clientes dados de alta en la DB
    CREATE TABLE cliente_count (clientes int);
    INSERT INTO cliente_count (clientes) VALUES(0);
    CREATE TRIGGER increment_cliente
    AFTER INSERT ON cliente 
    FOR EACH ROW 
    UPDATE cliente_count SET cliente_count.clientes = cliente_count.clientes+1;

    -- trigger para mantener la cuenta de proveedores dados de alta en la DB
    CREATE TABLE proveedor_count (proveedores int);
    INSERT INTO proveedor_count (proveedores) VALUES(0);
    CREATE TRIGGER increment_proveedor
    AFTER INSERT ON proveedor 
    FOR EACH ROW 
    UPDATE proveedor_count SET proveedor_count.proveedores = proveedor_count.proveedores+1;


-- Volcando datos para la tabla California.cliente: ~1 rows (aproximadamente)
/*!40000 ALTER TABLE `cliente` DISABLE KEYS */;
INSERT IGNORE INTO `cliente` (`id_cliente`, `Nombre`, `Email`, `Password`, `Telefono`, `RFC`) VALUES
	(1, 'Erik', 'borredrilo@gmail.com', "hola", '123456', 'GOHE2809985P4'),
	(2, 'Juan', 'JuanUAABD@uaa.mx', "hola", '12345678', 'JUAH281443'),
	(3, 'Carlos', 'carlosdolor@gmail.com', "hola", '123456789', 'CARL478531');
/*!40000 ALTER TABLE `cliente` ENABLE KEYS */;


-- Volcando datos para la tabla California.compra: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `compra` DISABLE KEYS */;
INSERT IGNORE INTO `compra` (`id_compra`, `id_proveedor`, `id_empleado`, `Fecha`, `Surtido`, `Total`) VALUES
	(1, 1, 2, '2019-11-28', 1, 10000),
	(2, 2, 2, '2019-11-28', 1, 5000),
	(3, 1, 1, '2019-11-28', 1, 7000),
	(4, 1, 2, '2019-11-28', 0, 2000),
	(5, 2, 1, '2019-11-28', 0, 6000);
/*!40000 ALTER TABLE `compra` ENABLE KEYS */;

-- Volcando datos para la tabla California.compraproducto: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `compraproducto` DISABLE KEYS */;
INSERT IGNORE INTO `compraproducto` (`id_producto`, `id_compra`, `Cantidad`, `Precio`) VALUES
	(5, 1, 100, 6000),
	(2, 1, 50, 4000),
	(4, 2, 110, 5000),
	(3, 4, 200, 2000),
	(1, 5, 250, 3540),
	(3, 5, 50, 2460),
	(4, 3, 500, 7000);
/*!40000 ALTER TABLE `compraproducto` ENABLE KEYS */;

-- Volcando datos para la tabla California.empleado: ~1 rows (aproximadamente)
/*!40000 ALTER TABLE `empleado` DISABLE KEYS */;
INSERT IGNORE INTO `empleado` (`id_empleado`, `Nombre`, `Puesto`, `id_Jefe`, `Telefono`, `Email`, `Username`, `Password`) VALUES
	(1, 'Kristian', 'Manager JR', 1, '1234567', 'kristian.alba@gmail.com', 'kristian', _binary 0x686F6C61),
	(2, 'Alejandra', 'Vendedor', 1, '1234567', 'alejandra.ponce@gmail.com', 'alejandra', _binary 0x686F6C61);
/*!40000 ALTER TABLE `empleado` ENABLE KEYS */;


-- Volcando datos para la tabla California.fruteriainfo: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `fruteriainfo` DISABLE KEYS */;
INSERT IGNORE INTO `fruteriainfo` (`Nombre`, `Direccion`, `RFC`) VALUES
	('Fruteria California', 'Arnulfo M. Valdéz 26, Centro Comercial Agropecuario, 20135 Aguascalientes, Ags.', 'CALI784596');
/*!40000 ALTER TABLE `fruteriainfo` ENABLE KEYS */;

-- Volcando datos para la tabla California.pedido: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `pedido` DISABLE KEYS */;
INSERT IGNORE INTO `pedido` (`id_pedido`, `id_cliente`, `id_empleado`, `Fecha`, `Estado`, `Surtido`, `Total`) VALUES
	(1, 1, 1, '2019-11-28', 'Creado', b'0', 500),
	(2, 3, 2, '2019-11-28', 'Creado', b'0', 2000),
	(3, 2, 1, '2019-11-28', 'Pagado', b'0', 200),
	(4, 1, 2, '2019-11-28', 'Pagado', b'1', 500);
/*!40000 ALTER TABLE `pedido` ENABLE KEYS */;

-- Volcando datos para la tabla California.pedidoproducto: ~1 rows (aproximadamente)
/*!40000 ALTER TABLE `pedidoproducto` DISABLE KEYS */;
INSERT IGNORE INTO `pedidoproducto` (`id_producto`, `id_pedido`, `Cantidad`, `Precio`) VALUES
	(1, 1, 4, 9),
	(3, 3, 6, 500),
	(4, 2, 3, 2000),
	(2, 4, 10, 500);
/*!40000 ALTER TABLE `pedidoproducto` ENABLE KEYS */;



-- Volcando datos para la tabla California.producto: ~1 rows (aproximadamente)
/*!40000 ALTER TABLE `producto` DISABLE KEYS */;
INSERT IGNORE INTO `producto` (`id_producto`, `Descripcion`, `CostoCompra`, `CostoVenta`, `id_productoCategoria`, `Existencia`, `PuntoReorden`, `UnidadMedida`, `Nombre`, `ruta`) VALUES
	(1, 'Platano', 4, 9, 1, 1000, 20, 'GRANEL', 'Platano','uploads/platano'),
	(2, 'Manzana', 3, 8, 1, 2000, 10, 'GRANEL', 'Manzana', 'uploads/manzana'),
	(3, 'Uva', 7, 15, 1, 1500, 50, 'GRANEL', 'Uva', 'uploads/uva'),
	(4, 'Calabaza', 4, 8, 2, 900, 10, 'GRANEL', 'Calabaza', 'uploads/calabaza'),
	(5, 'Fresa', 6, 12, 1, 1000, 25, 'PIEZA/UNIDAD', 'Fresa', 'uploads/fresa'),
	(6, 'Zanahoria', 2, 8, 2, 1500, 55, 'GRANEL', 'Zanahoria', 'uploads/zanahoria'),
    (7, 'Queso Asadero', 3, 81, 3, 1500, 55, 'GRANEL', 'Queso Asadero', 'uploads/quesoasadero'),
    (8, 'Nuez', 122, 240, 4, 1500, 55, 'GRANEL', 'Nuez', 'uploads/nuez');
/*!40000 ALTER TABLE `producto` ENABLE KEYS */;


-- Volcando datos para la tabla California.productocategoria: ~1 rows (aproximadamente)
/*!40000 ALTER TABLE `productocategoria` DISABLE KEYS */;
INSERT IGNORE INTO `productocategoria` (`id_productoCategoria`, `Descripcion`, `Nombre`) VALUES
	(1, 'Frutas', 'Fruta'),
	(2, 'Verduras', 'Verdura'),
    (3, 'Quesos', 'Queso'),
    (4, 'Semillas & Cereales', 'Semillas & Cereales');
/*!40000 ALTER TABLE `productocategoria` ENABLE KEYS */;


-- Volcando datos para la tabla California.proveedor: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `proveedor` DISABLE KEYS */;
INSERT IGNORE INTO `proveedor` (`id_proveedor`, `Nombre`, `Direccion`, `Telefono`, `Whatsapp`, `RFC`, `Email`) VALUES
	(1, 'Antonio', 'Francisco Villa', '4961290089', '4961290089', 'CIAA960923', 'crackboy999@gmail.com'),
	(2, 'Monica', 'Pabellon ', '1237485961', '7418529632', 'VALD741263', 'monicauaa@gmail.com');
/*!40000 ALTER TABLE `proveedor` ENABLE KEYS */;

-- Volcando datos para la tabla California.proveedorproductos: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `proveedorproductos` DISABLE KEYS */;
INSERT IGNORE INTO `proveedorproductos` (`id_proveedor`, `id_producto`) VALUES
	(1, 1),
	(1, 4),
	(1, 5),
	(1, 6),
	(2, 3),
	(2, 2),
	(2, 4),
	(2, 1),
	(1, 2),
	(1, 3),
	(2, 5),
	(2, 6);
/*!40000 ALTER TABLE `proveedorproductos` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
