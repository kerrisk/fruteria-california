DROP DATABASE IF EXISTS California;
CREATE DATABASE California;
USE California;

CREATE TABLE Producto (
    id_producto int(10) NOT NULL AUTO_INCREMENT,
    Descripcion varchar(255),
    CostoCompra float,
    CostoVenta float,
    id_productoCategoria int(10),
    Existencia float,
    PuntoReorden float,
    UnidadMedida varchar(255),
    Nombre varchar(255),
    PRIMARY KEY (id_producto)
    -- CONSTRAINT `ck_unidadMedida` 
    --     CHECK (UnidadMedida in ("Granel","Pieza/Unidad")),
    -- CONSTRAINT `fk_producto_prodCategoria`
    --     FOREIGN KEY (id_productoCategoria) REFERENCES ProductoCategoria (id_productoCategoria)
    --     ON DELETE RESTRICT
    --     ON UPDATE CASCADE    
);
CREATE TABLE ProductoCategoria (
    id_productoCategoria int(10),
    Descripcion varchar(255),
    Nombre varchar(255),
    PRIMARY KEY(id_productoCategoria)
);
CREATE TABLE ProductoImagen (
    id_productoImagen int(10),
    id_producto int(10) NOT NULL,
    ruta varchar(255),
    PRIMARY KEY (id_productoImagen)
    -- CONSTRAINT `fk_ProdImagen_Producto`
    --     FOREIGN KEY (id_producto) REFERENCES Producto (id_producto)
    --     ON DELETE CASCADE
    --     ON UPDATE CASCADE
);
CREATE TABLE Pedido (
    id_pedido int(10) NOT NULL AUTO_INCREMENT,
    id_cliente int(10) NOT NULL,
    id_empleado int(10) NOT NULL,
    Fecha date,
    Estado varchar(255),
    Surtido bit(1),
    Total float,
    PRIMARY KEY (id_pedido)
    -- CONSTRAINT `fk_pedido_cliente`
    --     FOREIGN KEY (id_cliente) REFERENCES Cliente (id_cliente)
    --     ON DELETE NO ACTION
    --     ON UPDATE CASCADE,
    -- CONSTRAINT `fk_pedido_empleado`
    --     FOREIGN KEY (id_empleado) REFERENCES Empleado (id_empleado)
    --     ON DELETE NO ACTION
    --     ON UPDATE CASCADE
);
CREATE TABLE Cliente (
    id_cliente int(10) NOT NULL AUTO_INCREMENT,
    Nombre varchar(255),
    Email varchar(255),
    Password blob,
    Telefono varchar(10),
    RFC varchar(13),
    PRIMARY KEY (id_cliente)
);
CREATE TABLE ClienteImagen (
    id_imagenCliente int(10),
    id_cliente int(10) NOT NULL,
    ruta varchar(255),
    PRIMARY KEY(id_imagenCliente)
    -- CONSTRAINT `fk_clienteImagen_cliente`
    --     FOREIGN KEY (id_cliente) REFERENCES Cliente (id_cliente)
    --     ON DELETE CASCADE
    --     ON UPDATE CASCADE
);
CREATE TABLE PedidoProducto (
    id_producto int(10) NOT NULL,
    id_pedido int(10),
    Cantidad float,
    Precio float
    -- CONSTRAINT `fk_pediProd_Producto` 
    --     FOREIGN KEY (id_producto) REFERENCES Producto (id_producto)
    --     ON DELETE NO ACTION
    --     ON UPDATE CASCADE,
    -- CONSTRAINT `fk_pediProd_Pedido` 
    --     FOREIGN KEY (id_pedido) REFERENCES Pedido (id_pedido)
    --     ON DELETE CASCADE
    --     ON UPDATE CASCADE
);
CREATE TABLE Empleado (
    id_empleado int(10) NOT NULL AUTO_INCREMENT,
    Nombre varchar(255),
    Puesto varchar(255),
    id_Jefe int(10) NOT NULL,
    Telefono varchar (10),
    Email varchar(255),
    Username varchar(255),  
    Password blob,
    PRIMARY KEY (id_empleado)
    -- CONSTRAINT `fk_empleado_jefe`
    --     FOREIGN KEY (id_Jefe) REFERENCES Empleado (id_empleado)
    --     ON DELETE NO ACTION
    --     ON UPDATE CASCADE
);
CREATE TABLE Proveedor (
    id_proveedor int(10) NOT NULL AUTO_INCREMENT,
    Nombre varchar(255),
    Direccion varchar(255),
    Telefono varchar(10),
    Whatsapp varchar(10),
    RFC varchar(13),
    Email varchar(255),
    PRIMARY KEY (id_proveedor)
);
CREATE TABLE Compra (
    id_compra int(10) NOT NULL AUTO_INCREMENT,
    id_proveedor int(10) NOT NULL,
    id_empleado int(10) NOT NULL,
    Fecha date,
    Surtido boolean,
    Total float,
    PRIMARY KEY (id_compra)
    -- CONSTRAINT `fk_compra_empleado`
    --     FOREIGN KEY (id_empleado) REFERENCES Empleado (id_empleado)
    --     ON DELETE NO ACTION
    --     ON UPDATE CASCADE,
    -- CONSTRAINT `fk_compra_proveedor`
    --     FOREIGN KEY (id_proveedor) REFERENCES Proveedor (id_proveedor)
    --     ON DELETE NO ACTION
    --     ON UPDATE CASCADE
);
CREATE TABLE CompraProducto (
    id_producto int(10) NOT NULL,
    id_compra int(10),
    Cantidad float,
    Precio float
    -- CONSTRAINT `fk_compraProd_Producto` 
    --     FOREIGN KEY (id_producto) REFERENCES Producto (id_producto)
    --     ON DELETE NO ACTION
    --     ON UPDATE CASCADE,
    -- CONSTRAINT `fk_compraProd_Compra`  
    --     FOREIGN KEY (id_compra) REFERENCES Compras (id_compra)
    --     ON DELETE CASCADE
    --     ON UPDATE CASCADE
);
CREATE TABLE ProveedorProductos (
    id_proveedor int(10) NOT NULL,
    id_producto int(10) NOT NULL
    -- CONSTRAINT `fk_proveProduct_Proveedor` 
    --     FOREIGN KEY (id_proveedor) REFERENCES Proveedor (id_proveedor)
    --     ON DELETE CASCADE
    --     ON UPDATE CASCADE,
    -- CONSTRAINT `fk_proveProduct_Producto` 
    --     FOREIGN KEY (id_producto) REFERENCES Producto (id_producto);
    --     ON DELETE CASCADE
    --     ON UPDATE CASCADE
);
CREATE TABLE FruteriaInfo (
    Nombre varchar(255),
    Direccion varchar(255),
    RFC varchar(255)
);

-- Producto Constraints
ALTER TABLE Producto ADD CONSTRAINT `ck_unidadMedida` CHECK (UnidadMedida in ("Granel","Pieza/Unidad"));
ALTER TABLE Producto ADD CONSTRAINT `fk_producto_prodCategoria` FOREIGN KEY (id_productoCategoria) REFERENCES ProductoCategoria (id_productoCategoria) ON DELETE RESTRICT ON UPDATE CASCADE;

--  ProductoImagen
ALTER TABLE ProductoImagen ADD CONSTRAINT `fk_ProdImagen_Producto` FOREIGN KEY (id_producto) REFERENCES Producto (id_producto) ON DELETE CASCADE ON UPDATE CASCADE;

-- Pedido
ALTER TABLE Pedido ADD CONSTRAINT `fk_pedido_cliente` FOREIGN KEY (id_cliente) REFERENCES Cliente (id_cliente) ON DELETE NO ACTION ON UPDATE CASCADE;
ALTER TABLE Pedido ADD CONSTRAINT `fk_pedido_empleado` FOREIGN KEY (id_empleado) REFERENCES Empleado (id_empleado) ON DELETE NO ACTION ON UPDATE CASCADE;

-- ClienteImagen
ALTER TABLE ClienteImagen ADD CONSTRAINT `fk_clienteImagen_cliente` FOREIGN KEY (id_cliente) REFERENCES Cliente (id_cliente) ON DELETE CASCADE ON UPDATE CASCADE;

-- PedidoProducto
ALTER TABLE PedidoProducto ADD CONSTRAINT `fk_pediProd_Producto` FOREIGN KEY (id_producto) REFERENCES Producto (id_producto) ON DELETE NO ACTION ON UPDATE CASCADE;
ALTER TABLE PedidoProducto ADD CONSTRAINT `fk_pediProd_Pedido` FOREIGN KEY (id_pedido) REFERENCES Pedido (id_pedido) ON DELETE CASCADE ON UPDATE CASCADE;

-- Empleado
ALTER TABLE Empleado ADD CONSTRAINT `fk_empleado_jefe` FOREIGN KEY (id_Jefe) REFERENCES Empleado (id_empleado) ON DELETE NO ACTION ON UPDATE CASCADE;

-- Compra
ALTER TABLE Compra ADD CONSTRAINT `fk_compra_empleado` FOREIGN KEY (id_empleado) REFERENCES Empleado (id_empleado) ON DELETE NO ACTION ON UPDATE CASCADE;
ALTER TABLE Compra ADD CONSTRAINT `fk_compra_proveedor` FOREIGN KEY (id_proveedor) REFERENCES Proveedor (id_proveedor) ON DELETE NO ACTION ON UPDATE CASCADE;

-- CompraProducto
ALTER TABLE CompraProducto ADD CONSTRAINT `fk_compraProd_Producto` FOREIGN KEY (id_producto) REFERENCES Producto (id_producto) ON DELETE NO ACTION ON UPDATE CASCADE;
ALTER TABLE CompraProducto ADD CONSTRAINT `fk_compraProd_Compra` FOREIGN KEY (id_compra) REFERENCES Compra (id_compra) ON DELETE CASCADE ON UPDATE CASCADE;

-- ProveedorProductos
ALTER TABLE ProveedorProductos ADD CONSTRAINT `fk_proveProduct_Proveedor` FOREIGN KEY (id_proveedor) REFERENCES Proveedor (id_proveedor) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE ProveedorProductos ADD CONSTRAINT `fk_proveProduct_Producto` FOREIGN KEY (id_producto) REFERENCES Producto (id_producto) ON DELETE CASCADE ON UPDATE CASCADE;

-- Volcando datos para la tabla California.Cliente: ~1 rows (aproximadamente)
/*!40000 ALTER TABLE `Cliente` DISABLE KEYS */;
INSERT IGNORE INTO `Cliente` (`id_Cliente`, `Nombre`, `Email`, `PASSWORD`, `Telefono`, `RFC`) VALUES
	(1, 'Erik', 'borredrilo@gmail.com', "hola", '123456', 'GOHE2809985P4'),
	(2, 'Juan', 'JuanUAABD@uaa.mx', "hola", '12345678', 'JUAH281443'),
	(3, 'Carlos', 'carlosdolor@gmail.com', "hola", '123456789', 'CARL478531');
/*!40000 ALTER TABLE `Cliente` ENABLE KEYS */;

-- Volcando datos para la tabla California.Compra: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `Compra` DISABLE KEYS */;
INSERT IGNORE INTO `Compra` (`id_compra`, `id_proveedor`, `id_empleado`, `Fecha`, `Surtido`, `Total`) VALUES
	(1, 1, 2, '2019-11-28', 1, 10000),
	(2, 2, 2, '2019-11-28', 1, 5000),
	(3, 1, 1, '2019-11-28', 1, 7000),
	(4, 1, 2, '2019-11-28', 0, 2000),
	(5, 2, 1, '2019-11-28', 0, 6000);
/*!40000 ALTER TABLE `Compra` ENABLE KEYS */;

-- Volcando datos para la tabla California.CompraProducto: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `CompraProducto` DISABLE KEYS */;
INSERT IGNORE INTO `CompraProducto` (`id_producto`, `id_compra`, `Cantidad`, `Precio`) VALUES
	(5, 1, 100, 6000),
	(2, 1, 50, 4000),
	(4, 2, 110, 5000),
	(3, 4, 200, 2000),
	(1, 5, 250, 3540),
	(3, 5, 50, 2460),
	(4, 3, 500, 7000);
/*!40000 ALTER TABLE `CompraProducto` ENABLE KEYS */;

-- Volcando datos para la tabla California.Empleado: ~1 rows (aproximadamente)
/*!40000 ALTER TABLE `Empleado` DISABLE KEYS */;
INSERT IGNORE INTO `Empleado` (`id_empleado`, `Nombre`, `Puesto`, `id_Jefe`, `Telefono`, `Email`, `Username`, `PASSWORD`) VALUES
	(1, 'Kristian', 'Manager JR', 1, '1234567', 'kristian.alba@gmail.com', 'kristian', _binary 0x686F6C61),
	(2, 'Alejandra', 'Vendedor', 1, '1234567', 'alejandra.ponce@gmail.com', 'alejandra', _binary 0x686F6C61);
/*!40000 ALTER TABLE `Empleado` ENABLE KEYS */;


-- Volcando datos para la tabla California.FruteriaInfo: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `FruteriaInfo` DISABLE KEYS */;
INSERT IGNORE INTO `FruteriaInfo` (`Nombre`, `Direccion`, `RFC`) VALUES
	('Fruteria California', 'Arnulfo M. Valdéz 26, Centro Comercial Agropecuario, 20135 Aguascalientes, Ags.', 'CALI784596');
/*!40000 ALTER TABLE `FruteriaInfo` ENABLE KEYS */;

-- Volcando datos para la tabla California.pedido: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `Pedido` DISABLE KEYS */;
INSERT IGNORE INTO `Pedido` (`id_pedido`, `id_cliente`, `id_empleado`, `Fecha`, `Estado`, `Surtido`, `Total`) VALUES
	(1, 1, 1, '2019-11-28', 'Creado', b'0', 500),
	(2, 3, 2, '2019-11-28', 'Creado', b'0', 2000),
	(3, 2, 1, '2019-11-28', 'Pagado', b'0', 200),
	(4, 1, 2, '2019-11-28', 'Pagado', b'1', 500);
/*!40000 ALTER TABLE `Pedido` ENABLE KEYS */;

/*!40000 ALTER TABLE `Producto` DISABLE KEYS */;
INSERT IGNORE INTO `Producto` (`id_producto`, `Descripcion`, `CostoCompra`, `CostoVenta`, `id_productoCategoria`, `Existencia`, `PuntoReorden`, `UnidadMedida`, `Nombre`) VALUES
	(1, 'Platano', 4, 9, 1, 1000, 20, 'GRANEL', 'Platano'),
	(2, 'Manzana', 3, 8, 1, 2000, 10, 'GRANEL', 'Manzana'),
	(3, 'Uva', 7, 15, 1, 1500, 50, 'GRANEL', 'Uva'),
	(4, 'Calabaza', 4, 8, 2, 900, 10, 'GRANEL', 'Calabaza'),
	(5, 'Fresa', 6, 12, 1, 1000, 25, 'PIEZA/UNIDAD', 'Fresa'),
	(6, 'Zanahoria', 2, 8, 2, 1500, 55, 'GRANEL', 'Zanahoria');
/*!40000 ALTER TABLE `Producto` ENABLE KEYS */;

-- Volcando datos para la tabla California.ProveedorProductos: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `ProveedorProductos` DISABLE KEYS */;
INSERT IGNORE INTO `ProveedorProductos` (`id_proveedor`, `id_producto`) VALUES
	(1, 1),
	(1, 4),
	(1, 5),
	(1, 6),
	(2, 3),
	(2, 2),
	(2, 4),
	(2, 1),
	(1, 2),
	(1, 3),
	(2, 5),
	(2, 6);
/*!40000 ALTER TABLE `ProveedorProductos` ENABLE KEYS */;