import { useContext } from "react";
import Navbar from "react-bootstrap/Navbar";
import Badge from "react-bootstrap/Badge";
import Link from "next/link";
import UserContext from "../UserContext";
import Button from "react-bootstrap/Button";

const FooterBar = () => {
  const { id_empleado, Nombre, signOutAdmin } = useContext(UserContext);
  const ButtonLogin = () => (
    <Link href="/Admin/Login">
      <Button variant="secondary" className="ml-2">
        <a>Log In</a>
      </Button>
    </Link>
  );

  const ButtonLogout = () => (
    <>
      <Navbar.Text>
        Signed in as: <a href="#login">{Nombre}</a>
      </Navbar.Text>
      <Button variant="danger" className="ml-2" onClick={signOutAdmin}>
        {"Log Out"}
      </Button>
    </>
  );
  return (
    <Navbar fixed="bottom" variant="light" bg="primary">
      <Badge pill variant="light">
        &copy; 2019 Frutería California S.A. De C.V.{" "}
      </Badge>
      <Navbar.Toggle />
      <Navbar.Collapse className="justify-content-end">
        {id_empleado ? <ButtonLogout /> : <ButtonLogin />}
      </Navbar.Collapse>
    </Navbar>
  );
};

export default FooterBar;
