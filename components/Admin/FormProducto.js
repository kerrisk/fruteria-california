import Container from "react-bootstrap/Container";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import Col from "react-bootstrap/Col";
import Router from "next/router";
import Layout from "../../components/Admin/Layout";
import { withRouter } from "next/router";
import ToastRight from "../../components/ToastRight";
import { useState, useContext, useEffect } from "react";
import Row from "react-bootstrap/Row";
const FormProducto = props => {
  const change = e => {
    // props.onChange({ [e.target.name]: e.target.value });
    props.setFormData({
      ...props.formData,
      [e.target.name]: e.target.value
    });
  };
  const onSubmit = e => {
    e.preventDefault();
    props.onSubmit(e);
    /* props.setFormData({
      id_producto: "",
      Descripcion: "",
      CostoCompra: "",
      CostoVenta: "",
      id_productoCategoria: "",
      Existencia: "",
      PuntoReorden: "",
      UnidadMedida: "",
      Nombre: ""
    }); */
  };
  return (
    <Container>
      <div>
        <h1>Producto id: {props.id_producto}</h1>
      </div>

      <Form
        onSubmit={event => onSubmit(event)}
        encType="multipart/form-data"
        method="POST"
        id="clienteRegistroForm"
        noValidate
      >
        <Form.Group
          as={Col}
          xs={12}
          md={8}
          className="mx-auto"
          controlId="Nombre"
        >
          <Form.Label>Nombre</Form.Label>
          <Form.Control
            type="text"
            placeholder="Ingrese del nombre del Producto"
            name="Nombre"
            value={props.formData.Nombre}
            onChange={e => change(e)}
            required
          />
        </Form.Group>
        <Form.Group
          as={Col}
          xs={12}
          md={8}
          className="mx-auto"
          controlId="Descripcion"
        >
          <Form.Label>Descripci{"ó"}n</Form.Label>
          <Form.Control
            type="text"
            placeholder="Ingrese la descripcion"
            name="Descripcion"
            value={props.formData.Descripcion}
            onChange={e => change(e)}
            required
          />
        </Form.Group>
        <Form.Group
          as={Col}
          xs={12}
          md={8}
          className="mx-auto"
          controlId="CostoCompra"
        >
          <Form.Label>Costo</Form.Label>
          <Form.Control
            type="text"
            placeholder="Ingrese costo"
            name="CostoCompra"
            value={props.formData.CostoCompra}
            onChange={e => change(e)}
            required
          />
        </Form.Group>
        <Form.Group
          as={Col}
          xs={12}
          md={8}
          className="mx-auto"
          controlId="CostoVenta"
        >
          <Form.Label>Precio publico</Form.Label>
          <Form.Control
            type="text"
            placeholder="Ingrese el precio publico"
            name="CostoVenta"
            value={props.formData.CostoVenta}
            onChange={e => change(e)}
            required
          />
        </Form.Group>
        <Form.Group
          as={Col}
          xs={12}
          md={8}
          className="mx-auto"
          controlId="id_productoCategoria"
        >
          <Form.Label>id_productoCategoria</Form.Label>
          <Form.Control
            type="text"
            placeholder="Ingrese el id de la categoria"
            name="id_productoCategoria"
            value={props.formData.id_productoCategoria}
            onChange={e => change(e)}
            required
          />
        </Form.Group>
        <Form.Group
          as={Col}
          xs={12}
          md={8}
          className="mx-auto"
          controlId="Existencia"
        >
          <Form.Label>Existencia</Form.Label>
          <Form.Control
            type="text"
            placeholder="Ingrese del la cantidad del producto"
            name="Existencia"
            value={props.formData.Existencia}
            onChange={e => change(e)}
            required
          />
        </Form.Group>
        <Form.Group
          as={Col}
          xs={12}
          md={8}
          className="mx-auto"
          controlId="PuntoReorden"
        >
          <Form.Label>PuntoReorden</Form.Label>
          <Form.Control
            type="text"
            placeholder="Ingrese del cuando se tiene que surtir mas producto"
            name="PuntoReorden"
            value={props.formData.PuntoReorden}
            onChange={e => change(e)}
            required
          />
        </Form.Group>
        <Form.Group
          as={Col}
          xs={12}
          md={8}
          className="mx-auto"
          controlId="UnidadMedida"
        >
          <Form.Label>Unidad/Medida</Form.Label>
          <Form.Control
            type="text"
            placeholder="Granel / Pieza"
            name="UnidadMedida"
            value={props.formData.UnidadMedida}
            onChange={e => change(e)}
            required
          />
        </Form.Group>
        <Form.Group
          as={Col}
          xs={12}
          md={8}
          className="mx-auto"
          controlId="avatar"
        >
          <Form.Label>Imagen</Form.Label>
          <Form.Control
            type="file"
            name="rutaI"
            // filename={props.formData.ruta}
            onChange={e => change(e)}
            required
          />
          <Form.Control
            type="hidden"
            name="ruta"
            value={props.formData.ruta}
            required
          />
        </Form.Group>
        <Form.Control
          type="hidden"
          name="id_producto"
          value={props.formData.id_producto}
          required
        />
        <Button
          disabled={props.isSubmitting}
          variant="primary"
          type="submit"
          className="float-right"
        >
          Registrar
        </Button>
      </Form>
    </Container>
  );
};

export default FormProducto;
