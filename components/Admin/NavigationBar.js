import Link from "next/link";
import Navbar from "react-bootstrap/Navbar";
import Nav from "react-bootstrap/Nav";
import NavDropdown from "react-bootstrap/NavDropdown";
import Form from "react-bootstrap/Form";
import FormControl from "react-bootstrap/FormControl";
import Button from "react-bootstrap/Button";
import Container from "react-bootstrap/Container";

const NavigationBar = () => (
  <Navbar collapseOnSelect expand="lg" bg="primary" variant="dark" expand="lg">
    <Container fluid>
      <Link href="/Admin" passHref>
        <Navbar.Brand>{"Frutería California"}</Navbar.Brand>
      </Link>
      <Navbar.Toggle aria-controls="basic-navbar-nav" />
      <Navbar.Collapse id="basic-navbar-nav">
        <Nav className="mr-auto">
          <Link href="/Admin/Productos" passHref>
            <Nav.Link>Productos</Nav.Link>
          </Link>
          <Link href="/Admin/Clientes" passHref>
            <Nav.Link>Clientes</Nav.Link>
          </Link>
          <Link href="/Admin/Proveedores" passHref>
            <Nav.Link>Proveedores</Nav.Link>
          </Link>
          <Link href="/Admin/Pedidos" passHref>
            <Nav.Link>Pedidos</Nav.Link>
          </Link>
        </Nav>
        <Form inline>
          <FormControl type="text" placeholder="Search" className="mr-sm-2" />
          <Button variant="secondary">Buscar</Button>
        </Form>
      </Navbar.Collapse>
    </Container>
  </Navbar>
);

export default NavigationBar;
