import Container from "react-bootstrap/Container";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import Col from "react-bootstrap/Col";
import Router from "next/router";
import Layout from "../../components/Admin/Layout";
import { withRouter } from "next/router";
import ToastRight from "../../components/ToastRight";
import { useState, useContext, useEffect } from "react";
import Row from "react-bootstrap/Row";
import FormProducto from "../../components/Admin/FormProducto";
import { MessageAlert } from "../MessageAlert";

const ProductoEditarForm = props => {
  const [isSubmitting, setSubmitting] = useState(false);
  const [showMsg, setShowMsg] = useState(false);
  const [MsgHead, setMsgHead] = useState("");
  const [MsgBody, setMsgBody] = useState("");
  const [MsgVariant, setMsgVariant] = useState("");
  const [formData, setFormData] = useState({
    id_producto: "",
    Descripcion: "",
    CostoCompra: "",
    CostoVenta: "",
    id_productoCategoria: "",
    Existencia: "",
    PuntoReorden: "",
    UnidadMedida: "",
    Nombre: "",
    ruta: ""
  });

  function handleSubmit(event) {
    event.preventDefault();
    setSubmitting(true);
    const data = new FormData(event.target);
    console.log(event.target);
    console.log(...data);
    fetch("/api/ProductoUpdate", {
      method: "POST",
      body: data
    })
      .then(resp => resp.json())
      .then(function(data) {
        if (data.status !== "OK") {
          setMsgHead(data.Code + " " + data.status);
          setMsgBody(data.Description);
          setShowMsg(true);
          setMsgVariant("danger");
        } else {
          //document.getElementById("clienteRegistroForm").reset();
          Router.push({
            pathname: "/Admin/Productos",
            query: {
              msg: true,
              MsgHead: "Exito",
              MsgBody: "Producto editado correctamente",
              MsgVariant: "primary"
            }
          });
        }
        //console.log(data.status);
      })
      .catch(err => {
        console.error(err);
      })
      .finally(final => {
        setSubmitting(false);
      });
  }
  useEffect(() => {
    console.log("mounted");
    console.log("getInitialProps Producto Editar Form " + props.id_producto);
    const res = fetch("/api/ProductoConsulta", {
      method: "POST",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify({ id_producto: props.id_producto })
    })
      .then(res => res.json())
      .then(data => {
        console.log(`Show data fetched. Count: ${data.length}`);
        console.log(data);
        setFormData(data.result[0]);
      })
      .catch(err => {
        console.error(err);
      });
  }, []);
  return (
    <>
      <MessageAlert
        f={{ showMsg: showMsg, setShowMsg: setShowMsg }}
        Heading={MsgHead}
        Body={MsgBody}
        variant={MsgVariant}
      />
      <FormProducto
        formData={formData}
        setFormData={setFormData}
        isSubmitting={isSubmitting}
        onSubmit={handleSubmit}
      />
      <pre>{JSON.stringify(formData, null, 2)}</pre>
    </>
  );
};

export default ProductoEditarForm;
