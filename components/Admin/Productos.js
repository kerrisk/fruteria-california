import React, { Component, useEffect, useState } from "react";
import fetch from "isomorphic-unfetch";
import Card from "react-bootstrap/Card";
import Button from "react-bootstrap/Button";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Table from "react-bootstrap/Table";
import Link from "next/link";

const Productos = props => {
  const [prod, setprod] = useState([]);
  useEffect(() => {
    console.log("mounted");
    console.log("getInitialProps Productos");
    const res = fetch("/api/ProductosConsulta", {
      method: "POST"
    })
      .then(res => res.json())
      .then(data => {
        console.log(`Show data fetched. Count: ${data.length}`);
        console.log(data);
        setprod(data.result);
      })
      .catch(err => {
        console.error(err);
      });
  }, []);

  return (
    <Container fluid>
      <Row>
        <Link href="/Admin/ProductoAgregar">
          <Button className="ml-auto mr-3">
            <a>Agregar</a>
          </Button>
        </Link>
        <Table striped bordered hover>
          <thead>
            <tr>
              <th>#</th>
              <th>Nombre</th>
              <th>Descripci{"ó"}n</th>
              <th>Costo</th>
              <th>Costo Publico</th>
              <th>Categoria</th>
              <th>Existencia</th>
              <th>Punto Reorden</th>
              <th>Unidad/Medida</th>
              <th>Editar</th>
              <th>Eliminar</th>
            </tr>
          </thead>
          <tbody>
            {prod.map((producto, index) => {
              return (
                <tr key={producto.id_producto}>
                  <td>{producto.id_producto}</td>
                  <td>{producto.Nombre}</td>
                  <td>{producto.Descripcion}</td>
                  <td>{producto.CostoCompra}</td>
                  <td>{producto.CostoVenta}</td>
                  <td>{producto.id_productoCategoria}</td>
                  <td>{producto.Existencia}</td>
                  <td>{producto.PuntoReorden}</td>
                  <td>{producto.UnidadMedida}</td>
                  <td>
                    <Link
                      href={{
                        pathname: "/Admin/ProductoEditar",
                        query: { id_producto: producto.id_producto }
                      }}
                    >
                      <Button variant="light">
                        <a>Editar</a>
                      </Button>
                    </Link>
                  </td>
                  <td>
                    <Link
                      href={{
                        pathname: "/Admin/ProductoEliminar",
                        query: { id_producto: producto.id_producto }
                      }}
                    >
                      <Button variant="danger">
                        <a>Eliminar</a>
                      </Button>
                    </Link>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </Table>
      </Row>
    </Container>
  );
};
export default Productos;
