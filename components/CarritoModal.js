//modulo de carrito de compras

import { useState } from "react";
import Link from "next/link";
import Button from "react-bootstrap/Button";
import Modal from "react-bootstrap/Modal";
import Form from "react-bootstrap/Form";

function handleSubmit(event, handleClose) {
  console.log("Form submited");
  event.preventDefault();
  const data = new FormData(event.target);
  //console.log(event.target);
  console.log(...data);
  fetch("/Carrito", {
    method: "POST",
    body: data
  });
  handleClose();
}

const CarritoModal = () => {
  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  return (
    <>
      <h4>Carrito de compras</h4>
    </>
  );
};

export default CarritoModal;
