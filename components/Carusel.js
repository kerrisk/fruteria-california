import Carousel from "react-bootstrap/Carousel";
const Carusel = () => {
  const pics = [
    /*   "F1",
    "F2",
    "F3",
    "F4",
    "F5",
    "F6",
    "F7",
    "F8",
    "F9",
    "F10",
    "F11",
    "F12" */
    "F8"
  ];
  return (
    <Carousel className="mx-auto" fade>
      {pics.map((value, index) => {
        {
          console.log(value);
        }
        return (
          <Carousel.Item>
            <img
              key={value}
              className="d-block w-100"
              src={"uploads/" + value}
              alt={index + " slide"}
            />
            {/* <Carousel.Caption>
              <h3>First slide label</h3>
              <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>
            </Carousel.Caption> */}
          </Carousel.Item>
        );
      })}
    </Carousel>
  );
};

export default Carusel;
