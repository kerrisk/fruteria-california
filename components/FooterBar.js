import { useContext } from "react";
import Navbar from "react-bootstrap/Navbar";
import Badge from "react-bootstrap/Badge";
import Link from "next/link";
import UserContext from "./UserContext";
import Button from "react-bootstrap/Button";

const FooterBar = () => {
  const { Nombre, signOut } = useContext(UserContext);
  const ButtonLogin = () => (
    <Link href="/Login">
      <Button variant="secondary" className="ml-2">
        <a>Log In</a>
      </Button>
    </Link>
  );

  const ButtonLogout = () => (
    <>
      <Navbar.Text>
        Signed in as: <a href="#login">{Nombre}</a>
      </Navbar.Text>
      <Button variant="danger" className="ml-2" onClick={signOut}>
        {"Log Out"}
      </Button>
    </>
  );
  return (
    <Navbar fixed="bottom" variant="light" bg="primary">
      <Badge pill variant="light">
        &copy; 2019 Frutería California S.A. De C.V.{" "}
      </Badge>
      <Navbar.Toggle />
      <Navbar.Collapse className="justify-content-end">
        {Nombre ? <ButtonLogout /> : <ButtonLogin />}
      </Navbar.Collapse>
    </Navbar>
  );
};

export default FooterBar;
