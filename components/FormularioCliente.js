import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import { useEffect } from "react";
import dynamic from "next/dynamic";
const Spinner = dynamic(() => import("react-spinner-material"), { ssr: false });
const FormularioCliente = props => {
  const handleSubmit = props.handleSubmit;
  var Regresar = () => "";
  var ClienteID = () => "";
  var hidden = "hidden",
    nombre = "";
  useEffect(() => {
    if (props.Operacion === "Update") {
      props.setloading(true);
      const getClienteData = props.getClienteData(
        props.clientID,
        props.setloading
      );
      console.log("Operacion = " + props.Operacion);
      Regresar = () => {
        return (
          <Button variant="info" onClick={props.regresar}>
            Regresar
          </Button>
        );
      };
      ClienteID = () => {
        return (
          <Form.Group controlId="clientID">
            <Form.Label>Id Cliente</Form.Label>
            <Form.Control
              type="text"
              placeholder="#0"
              name="clientID"
              disabled
              value={props.clientID}
            />
          </Form.Group>
        );
      };
    }
  });

  useEffect(() => {
    console.log(props.loading);
    if (props.loading === false) {
      hidden = "";
      nombre = "Loaded";
    }
  }, [props.loading]);
  return (
    <>
      <Spinner
        size={120}
        spinnerColor={"#333"}
        spinnerWidth={2}
        visible={props.loading}
      />
      <Form
        onSubmit={event => handleSubmit(event)}
        encType="multipart/form-data"
        method="POST"
        className={hidden}
        noValidate
      >
        <ClienteID />
        <Form.Group controlId="registerName">
          <Form.Label>Nombre</Form.Label>
          <Form.Control
            type="text"
            placeholder="Ingrese su Nombre"
            name="registerName"
            value={nombre}
            required
          />
        </Form.Group>
        <Form.Group controlId="registerImage">
          <Form.Label>Ingrese imagen suya</Form.Label>
          <Form.Control
            type="file"
            name="registerImage"
            className="file"
            required
          />
        </Form.Group>

        <Form.Group controlId="registerEmail">
          <Form.Label>Correo Electrónico</Form.Label>
          <Form.Control
            type="email"
            placeholder="Ingrese su correo electrónico"
            name="registerEmail"
            required
          />
        </Form.Group>
        <Form.Group controlId="registerNumber">
          <Form.Label>Numero de Telefono</Form.Label>
          <Form.Control
            type="number"
            placeholder="Ingrese su numero de Telefono (10 dígitos)"
            name="registerNumber"
            required
          />
        </Form.Group>
        <Form.Group controlId="registerPassword">
          <Form.Label>Contraseña</Form.Label>
          <Form.Control
            type="password"
            placeholder="Ingrese su Contraseña"
            name="registerPassword"
            required
          />
        </Form.Group>
        <Form.Group controlId="registerPasswordCheck">
          <Form.Label>Repita la Contraseña</Form.Label>
          <Form.Control
            type="password"
            placeholder="Ingrese de nuevo su Contraseña"
            name="registerPasswordCheck"
            required
          />
        </Form.Group>
        <Button variant="primary" type="submit" className="float-right">
          Registrar
        </Button>
        <Regresar />
      </Form>
    </>
  );
};

export default FormularioCliente;
