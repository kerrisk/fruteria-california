import Head from "next/head";
import NavigationBar from "./NavigationBar";
import FooterBar from "./FooterBar";
import Container from "react-bootstrap/Container";

const Layout = props => {
  return (
    <>
      <Head>
        <title>Frutería California</title>
        <link rel="stylesheet" href="css/bootstrap.min.css" />
        <link rel="stylesheet" href="css/style.css" />
      </Head>
      <div>
        <NavigationBar />
        <Container fluid>{props.children}</Container>
        <FooterBar />
      </div>
    </>
  );
};

export default Layout;
