import { useState, Component } from "react";
import Link from "next/link";
import Button from "react-bootstrap/Button";
import Modal from "react-bootstrap/Modal";
import Form from "react-bootstrap/Form";
var UserCorrect = true;
var reg = "Inicio";
function handleSubmit(event, handleClose, handleLogged) {
  console.log("Form submited");
  event.preventDefault();
  const data = new FormData(event.target);
  //console.log(event.target);
  console.log(...data);
  fetch("/profile", {
    method: "POST",
    body: data
  })
    .then(resp => resp.json())
    .then(function(data) {
      console.log(data.status);
      if (data.status == "Success") {
        UserCorrect = true;
        reg = data.msg;
        handleLogged();
        handleClose();
      } else {
        UserCorrect = false;
      }
    });

  //handleClose();
}

const LoginModal = () => {
  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);
  const handleLogged = () => setShow(false);
  return (
    <>
      <Button variant="danger" className="ml-2" onClick={handleShow}>
        {reg}
      </Button>
      <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>Login</Modal.Title>
        </Modal.Header>

        <Modal.Body>
          <Form
            onSubmit={event => handleSubmit(event, handleClose, handleLogged)}
            encType="multipart/form-data"
          >
            <Form.Group controlId="loginUsername">
              <Form.Label>Usuario</Form.Label>
              <Form.Control
                type="text"
                placeholder="Ingresa tu usuario"
                name="loginUsername"
                required
              />
            </Form.Group>
            <Form.Group controlId="loginPassword">
              <Form.Label>Contraseña</Form.Label>
              <Form.Control
                type="password"
                placeholder="Contraseña"
                name="loginPassword"
                required
              />
            </Form.Group>
            {/* <Form.Group controlId="formBasicCheckbox">
              <Form.Check type="checkbox" label="Check me out" />
            </Form.Group> */}
            <Button variant="primary" type="submit">
              Submit
            </Button>
          </Form>
        </Modal.Body>
        <label hidden={UserCorrect}>Usuario Incorrrecto</label>
        <Modal.Footer>
          <Link href="/ClienteRegistro">
            <a>¿No tienes una cuenta? Registrate Aquí!</a>
          </Link>
          <Button variant="secondary" onClick={handleClose}>
            Close
          </Button>
          {/* <Button variant="primary" onClick={handleClose}>
            Save Changes
          </Button> */}
        </Modal.Footer>
      </Modal>
    </>
  );
};

export default LoginModal;
