import Alert from "react-bootstrap/Alert";
export const MessageAlert = props => {
  if (props.f.showMsg) {
    return (
      <Alert
        variant={props.variant}
        onClose={() => props.f.setShowMsg(false)}
        dismissible
      >
        <Alert.Heading>{props.Heading}</Alert.Heading>
        <p>{props.Body}</p>
      </Alert>
    );
  }
  return "";
};
export default MessageAlert;
