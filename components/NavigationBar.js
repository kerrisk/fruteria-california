import Link from "next/link";
import Navbar from "react-bootstrap/Navbar";
import Nav from "react-bootstrap/Nav";
import NavDropdown from "react-bootstrap/NavDropdown";
import Form from "react-bootstrap/Form";
import FormControl from "react-bootstrap/FormControl";
import Button from "react-bootstrap/Button";
import Container from "react-bootstrap/Container";

const NavigationBar = () => (
  <Navbar collapseOnSelect expand="lg" bg="primary" variant="dark" expand="lg">
    <Container fluid>
      <Link href="/" passHref>
        <Navbar.Brand>{"Frutería California"}</Navbar.Brand>
      </Link>
      <Navbar.Toggle aria-controls="basic-navbar-nav" />
      <Navbar.Collapse id="basic-navbar-nav">
        <Nav className="mr-auto">
          <Link href="/" passHref>
            <Nav.Link>Inicio</Nav.Link>
          </Link>
          <Link href="/aboutUs" passHref>
            <Nav.Link>Nosotros</Nav.Link>
          </Link>
          <NavDropdown title="Productos" id="basic-nav-dropdown">
            <Link href="/frutas" passHref>
              <NavDropdown.Item>Frutas</NavDropdown.Item>
            </Link>
            <Link href="/verduras" passHref>
              <NavDropdown.Item>Verduras</NavDropdown.Item>
            </Link>
            <Link href="/semillas" passHref>
              <NavDropdown.Item>Semillas & Cereales</NavDropdown.Item>
            </Link>
            <NavDropdown.Divider />
            <Link href="/quesos" passHref>
              <NavDropdown.Item>Quesos</NavDropdown.Item>
            </Link>
          </NavDropdown>
        </Nav>
        <Form inline>
          <FormControl type="text" placeholder="Search" className="mr-sm-2" />
          <Button variant="secondary">Buscar</Button>
        </Form>
      </Navbar.Collapse>
    </Container>
  </Navbar>
);

export default NavigationBar;
