import React, { Component, useEffect, useState } from "react";
import fetch from "isomorphic-unfetch";
import Card from "react-bootstrap/Card";
import Button from "react-bootstrap/Button";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";

const Productos = props => {
  const [prod, setprod] = useState([]);
  useEffect(() => {
    console.log("mounted");
    console.log("getInitialProps Productos");
    console.log(props.id_productoCategoria);
    const res = fetch("/api/ProductosConsultaCat", {
      method: "POST",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify({ id_productoCategoria: props.id_productoCategoria })
    })
      .then(res => res.json())
      .then(data => {
        console.log(`Show data fetched. Count: ${data.length}`);
        console.log(data);
        setprod(data.result);
      })
      .catch(err => {
        console.error(err);
      });
  }, []);
  return (
    <Container fluid>
      <h2>Productos</h2>
      <Row>
        {prod.map((producto, index) => {
          return (
            <Card
              style={{ width: "18rem" }}
              key={producto.id_producto}
              className="mx-auto my-3"
            >
              <Card.Img variant="top" src={producto.ruta} />
              <Card.Body>
                <Card.Title>{producto.Nombre}</Card.Title>
                <Card.Text>{producto.Descripcion}</Card.Text>
                <Card.Text>Precio: ${producto.CostoVenta}</Card.Text>
                <Card.Text>Existencia: {producto.Existencia}</Card.Text>
                <Card.Text>Vendido por: {producto.UnidadMedida}</Card.Text>
                <Button variant="primary">Go somewhere</Button>
              </Card.Body>
              {/* <div>{JSON.stringify(producto, null, 2)}</div> */}
            </Card>
          );
        })}
      </Row>
    </Container>
  );
};
export default Productos;
