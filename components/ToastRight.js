import Toast from "react-bootstrap/Toast";

const ToastRight = props => {
  return (
    <Toast
      style={{
        position: "absolute",
        top: 0,
        right: 0
      }}
      onClose={() => props.f.setShowMsg(false)}
      show={props.f.showMsg}
      delay={3000}
    >
      <Toast.Header>
        {/* <img src="holder.js/20x20?text=%20" className="rounded mr-2" alt="" /> */}
        <strong className="mr-auto">{props.Heading}</strong>
        <small>just now</small>
      </Toast.Header>
      <Toast.Body>{props.Body}</Toast.Body>
    </Toast>
  );
};

export default ToastRight;
