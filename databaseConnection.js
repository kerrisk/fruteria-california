var mysql = require("mysql");

//Crea la conexion con la base de datos
var con = mysql.createPool({
  connectionLimit: 10,
  host: "localhost",
  user: "root",
  password: "hola",
  database: "California"
});

//console.log(process.env);
con.on("connection", function(connection) {
  connection.on("enqueue", function(sequence) {
    // if (sequence instanceof mysql.Sequence.Query) {
    if ("Query" === sequence.constructor.name) {
      console.log(sequence.sql);
    }
  });
});
module.exports = con;
