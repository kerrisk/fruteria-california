// home page route (http://localhost:8080)
router.post("/Login", function(req, res) {
  var resp;
  var query =
    'SELECT 1 FROM Administradores WHERE username = "' +
    req.body.username +
    '" AND password = "' +
    req.body.password +
    '" ORDER BY Username LIMIT 1';
  con.query(query, function(err, results, fields) {
    if (err) {
      console.log(
        "------------------------------------Database ERROR-------------------------------------"
      );
      console.log("this.sql", this.sql); //command/query
      console.log(err);
      console.log(
        "---------------------------------------------------------------------------------------"
      );
      return;
    }
    console.log("Resultado Query");
    console.log(results.length);
    console.log(results);
    resp = results.length;
    console.log("Consulta completada sin errores");
    if (resp == "1") {
      //En caso de existir el usuario
      console.log(resp);
      console.log("Adentro");
      tipo = 1;
      req.session.username = req.body.username;
      req.session.password = req.body.password;
      var respuesta = {
        status: 1,
        tipoA: 1,
        mensaje: "Sesion Iniciada, Bienvenido"
      };
    } else {
      console.log("Usuario no encontrado");
      var respuesta = {
        status: 0,
        mensaje: "Error Contraseña y/o usuario"
      };
    }
    console.log(respuesta);
    res.setHeader("Content-Type", "application/json");
    res.end(JSON.stringify(respuesta));
  });
});

router.post("/Registro", function(req, res) {
  var query =
    'INSERT INTO Productos VALUES (NULL,"' +
    req.body.name +
    '","' +
    req.body.priceCom +
    '","' +
    req.body.priceVen +
    '","' +
    req.body.cantInv +
    '");';
  console.log(query);
  con.query(query, function(err, result) {
    if (err) {
      console.log("this.sql", this.sql); //command/query
      // console.log(command);
      console.log("ERROR");
      console.log(err);

      return;
    }
    console.log("Consulta completada sin errores");
  });
  console.log(req.body.name + ": registro exitoso");
  res.setHeader("Content-Type", "application/json");
  res.end(
    JSON.stringify({
      status: "ProductoAlta"
    })
  );
});

router.post("/ProdActualizar", function(req, res) {
  var query =
    "UPDATE Productos SET Nombre='" +
    req.body.Nombre +
    "', precioCompra=" +
    req.body.precioCompra +
    ", precioVenta=" +
    req.body.precioVenta +
    ", Cantidad=" +
    req.body.Cantidad +
    " WHERE id = " +
    req.body.id +
    "";
  console.log(query);
  con.query(query, function(err, result) {
    if (err) {
      console.log("this.sql", this.sql); //command/query
      // console.log(command);
      console.log("ERROR");
      console.log(err);
      return;
    }
    console.log("Consulta completada sin errores");
  });
  console.log(req.body.Nombre + ": registro actualizado");
  res.setHeader("Content-Type", "application/json");
  res.end(
    JSON.stringify({
      status: "ProductoActualizado"
    })
  );
});

router.post("/ProdEliminar", function(req, res) {
  var query = "DELETE FROM Productos WHERE id = " + req.body.id + "";
  console.log(query);
  con.query(query, function(err, result) {
    if (err) {
      console.log("this.sql", this.sql); //command/query
      // console.log(command);
      console.log("ERROR");
      console.log(err);
      return;
    }
    console.log("Consulta completada sin errores");
  });
  console.log(req.body.Nombre + ": registro eliminado");
  res.setHeader("Content-Type", "application/json");
  res.end(
    JSON.stringify({
      status: "ProductoEliminado"
    })
  );
});

router.get("/idProduct", function(req, res) {
  var respuesta = [];
  var query = "SELECT * FROM Productos;";
  con.query(query, function(err, result) {
    if (err) {
      console.log(
        "------------------------------------Database ERROR-------------------------------------"
      );
      console.log("this.sql", this.sql); //command/query
      console.log(err);
      console.log(
        "---------------------------------------------------------------------------------------"
      );
      return;
    } else {
      //console.log(result);
      //console.log(result.data);
      //console.log(result[0]);
      for (var i = 0; i < result.length; i++) {
        respuesta.push({
          id: result[i].id
        });
      }
      console.log(respuesta);
      res.setHeader("Content-Type", "application/json");
      res.end(JSON.stringify(result));
    }
  });
});

router.get("/Producto/:id", function(req, res) {
  console.log(req.params.id);
  var respuesta = [];
  var query = "SELECT * FROM Productos WHERE id = " + req.params.id + ";";
  con.query(query, function(err, result) {
    if (err) {
      console.log(
        "------------------------------------Database ERROR-------------------------------------"
      );
      console.log("this.sql", this.sql); //command/query
      console.log(err);
      console.log(
        "---------------------------------------------------------------------------------------"
      );
      return;
    } else {
      console.log(result);
      res.setHeader("Content-Type", "application/json");
      res.end(JSON.stringify(result));
    }
  });
});

router.post("/InPac", function(req, res) {
  console.log(req.body); // populated!
  //Crea la consulta para insertar el medico
  var query = 'Select * FROM Pacientes WHERE Id =  "' + req.body.id + '";';
  console.log(query);
  con.query(query, function(err, result) {
    if (err) {
      console.log(
        "------------------------------------Database ERROR-------------------------------------"
      );
      console.log("this.sql", this.sql); //command/query
      console.log(err);
      console.log(
        "---------------------------------------------------------------------------------------"
      );
      return;
    }
    console.log("Consulta completada sin errores");
    console.log("busqueda exitosa");
    res.setHeader("Content-Type", "application/json");
    res.end(
      JSON.stringify({
        status: "BusEx",
        datos: JSON.stringify(result)
      })
    );
  });
});

router.get("/Verificar/:codigo", function(req, res) {
  console.log(req.params.codigo);
  //Actualiza la cuenta con el codigo de verificacion para que pueda iniciar sesion
  var query =
    'UPDATE Medicos SET Confirmado = 1 WHERE CodigoConfirmacion = "' +
    req.params.codigo +
    '";';
  console.log(query);
  con.query(query, function(err, result) {
    if (err) {
      console.log(
        "------------------------------------Database ERROR-------------------------------------"
      );
      console.log("this.sql", this.sql); //command/query
      console.log(err);
      console.log(
        "---------------------------------------------------------------------------------------"
      );
      return;
    } else {
      console.log("Cuenta verificada");
      res.redirect("/");
      //res.setHeader('Content-Type', 'application/json');
      //res.end(JSON.stringify({status: 'Cuenta verificada' }));
    }
  });
});

router.get("/Logout", function(req, res) {
  // if the user logs out, destroy all of their individual session
  // information
  req.session.destroy(function(err) {
    if (err) {
      console.log(err);
    } else {
      res.redirect("/");
    }
  });
});
