import Layout from "../components/Layout";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import Container from "react-bootstrap/Container";
import { useState, useEffect } from "react";
import FormularioCliente from "../components/FormularioCliente.js";

function handleSubmitConsult(event, selectUser) {
  console.log("Form submited");
  event.preventDefault();
  const data = new FormData(event.target);
  //console.log(event.target);
  console.log(...data);
  console.log(data.get("idCliente"));
  selectUser(data.get("idCliente"));
}
function handleSubmit(event, selectUser) {
  console.log("Form submited");
  event.preventDefault();
  const data = new FormData(event.target);
  //console.log(event.target);
  console.log(...data);
  console.log(data.get("idCliente"));
  selectUser(data.get("idCliente"));
}
function getClienteData(UserSelected, setloading) {
  setloading(true);
  fetch("/api/clientConsult", {
    method: "POST",
    body: JSON.stringify({ clientID: UserSelected }),
    headers: {
      "Content-Type": "application/json"
    }
  })
    .then(resp => resp.json())
    .then(function(data) {
      console.log(data);
      setloading(false);
    });
}
const ActualizarCliente = () => {
  const [UserSelected, selectUser] = useState(0);

  return (
    <Layout>
      <Container>
        <div>
          <h1>Editar cliente</h1>
          <Form
            //onSubmit={event => handleSubmitConsult(event, selectUser)}
            //encType="multipart/form-data"
            method="get"
            action="/ActualizarClienteEdit"
          >
            <Form.Group controlId="idCliente">
              <Form.Label>Id Cliente:</Form.Label>
              <Form.Control
                type="text"
                placeholder="Ingrese el id del cliente"
                name="idCliente"
                required
              />
            </Form.Group>
            <Button variant="primary" type="submit" className="float-right">
              Consultar
            </Button>
          </Form>
        </div>
      </Container>
    </Layout>
  );
};

export default ActualizarCliente;
