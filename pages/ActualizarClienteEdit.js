import Layout from "../components/Layout";
import fetch from "isomorphic-unfetch";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import Container from "react-bootstrap/Container";
import { useState, useEffect } from "react";
import Router from "next/router";
import FormularioCliente from "../components/FormularioCliente.js";

function handleSubmitConsult(event, selectUser) {
  console.log("Form submited");
  event.preventDefault();
  const data = new FormData(event.target);
  //console.log(event.target);
  console.log(...data);
  console.log(data.get("idCliente"));
  selectUser(data.get("idCliente"));
}
function handleSubmit(event, selectUser) {
  console.log("Form submited");
  event.preventDefault();
  const data = new FormData(event.target);
  //console.log(event.target);
  console.log(...data);
  console.log(data.get("idCliente"));
  selectUser(data.get("idCliente"));
}
function getClienteData(UserSelected, setloading) {
  setloading(true);
  fetch("/api/clientConsult", {
    method: "POST",
    body: JSON.stringify({ clientID: UserSelected }),
    headers: {
      "Content-Type": "application/json"
    }
  })
    .then(resp => resp.json())
    .then(function(data) {
      console.log(data);
      setloading(false);
    });
}
const ActualizarCliente = props => {
  const [loading, setloading] = useState(true);
  const resetUser = () => {
    selectUser(0);
  };
  console.log(props);
  if (props.data.result[0] === undefined) {
    console.log("No result");
    Router.push({
      pathname: "/ActualizarCliente",
      query: { error: "Cliente No Encontrado!" }
    });
  }
  return (
    <Layout>
      <Container>
        <div>
          <h1>Editar cliente</h1>
          <Form
            onSubmit={event => handleSubmit(event)}
            encType="multipart/form-data"
            method="POST"
            noValidate
          >
            <Form.Group controlId="registerId">
              <Form.Label>Cliente Id</Form.Label>
              <Form.Control
                type="text"
                placeholder="Ingrese su Nombre"
                name="registerId"
                defaultValue={props.data.result[0].id_cliente}
                required
              />
            </Form.Group>
            <Form.Group controlId="registerName">
              <Form.Label>Nombre</Form.Label>
              <Form.Control
                type="text"
                placeholder="Ingrese su Nombre"
                name="registerName"
                defaultValue={props.data.result[0].Nombre}
                required
              />
            </Form.Group>
            <Form.Group controlId="registerImage">
              <Form.Label>Ingrese imagen suya</Form.Label>
              <Form.Control
                type="file"
                name="registerImage"
                className="file"
                required
              />
            </Form.Group>

            <Form.Group controlId="registerEmail">
              <Form.Label>Correo Electrónico</Form.Label>
              <Form.Control
                type="email"
                placeholder="Ingrese su correo electrónico"
                name="registerEmail"
                defaultValue={props.data.result[0].Email}
                required
              />
            </Form.Group>
            <Form.Group controlId="registerNumber">
              <Form.Label>Numero de Telefono</Form.Label>
              <Form.Control
                type="number"
                placeholder="Ingrese su numero de Telefono (10 dígitos)"
                name="registerNumber"
                defaultValue={props.data.result[0].Telefono}
                required
              />
            </Form.Group>
            <Form.Group controlId="registerRFC">
              <Form.Label>RFC</Form.Label>
              <Form.Control
                type="number"
                placeholder="Ingrese su RFC"
                name="registerRFC"
                defaultValue={props.data.result[0].RFC}
                required
              />
            </Form.Group>
            <Form.Group controlId="registerPassword">
              <Form.Label>Contraseña</Form.Label>
              <Form.Control
                type="password"
                placeholder="Ingrese su Contraseña"
                name="registerPassword"
                required
              />
            </Form.Group>
            <Button variant="primary" type="submit" className="float-right">
              Registrar
            </Button>
          </Form>
        </div>
      </Container>
    </Layout>
  );
};
ActualizarCliente.getInitialProps = async function(res) {
  /* const res = await  */
  console.log("Query----------");
  console.log(res.query);
  console.log(res.query.idCliente);
  console.log("FINNNNNN Query----------");
  const result = await fetch("http://localhost:3333/api/ConsultaCliente/", {
    method: "POST",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json"
    },
    body: JSON.stringify({ idCliente: res.query.idCliente })
  });
  const data = await result.json();
  //console.log(data);

  return { data };
};
export default ActualizarCliente;
