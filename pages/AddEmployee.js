import Layout from "../components/Layout";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import Container from "react-bootstrap/Container";

function handleSubmit(event) {
  console.log("Form submited");
  event.preventDefault();
  const data = new FormData(event.target);
  //console.log(event.target);
  console.log(...data);
  fetch("/addEmployee", {
    method: "POST",
    body: data
  });
  handleSuccesfullySubmit();
}

function handleSuccesfullySubmit() {
  console.log("Submited");
}
const AddEmployee = () => (
  <Layout>
    <Container>
      <div>
        <h1>Registro de nuevo empleado</h1>
      </div>
      <Form
        onSubmit={event => handleSubmit(event)}
        encType="multipart/form-data"
      >
        <Form.Group controlId="EmpName">
          <Form.Label>Nombre</Form.Label>
          <Form.Control
            type="text"
            placeholder="Ingrese Nombre del Empleado"
            name="EmpName"
            required
          />
        </Form.Group>
        <Form.Group controlId="EmpPuest">
          <Form.Label>Puesto</Form.Label>
          <Form.Control
            type="text"
            placeholder="Ingrese puesto del empleado"
            name="EmpPuest"
            required
          />
        </Form.Group>
        <Form.Group controlId="EmpTel">
          <Form.Label>Numero de Telefono</Form.Label>
          <Form.Control
            type="number"
            placeholder="Ingrese el numero de telefono del empleado"
            name="prodCost"
            required
          />
        </Form.Group>
        <Form.Group controlId="EmpEmail">
          <Form.Label>Email</Form.Label>
          <Form.Control
            type="email"
            placeholder="Ingrese el email del empleado"
            name="EmpEmail"
            required
          />
        </Form.Group>
        <Form.Group controlId="EmpUsername">
          <Form.Label>Nombre de Usuario</Form.Label>
          <Form.Control
            type="text"
            placeholder="Ingrese el nombre de usuario del empleado"
            name="EmpUsername"
            required
          />
        </Form.Group>

        <Form.Group controlId="EmpPassword">
          <Form.Label>Contraseña</Form.Label>
          <Form.Control
            type="text"
            placeholder="Ingrese la contraseña del empleado"
            name="EmpPassword"
            required
          />
        </Form.Group>

        {/* <Form.Group controlId="formBasicCheckbox">
              <Form.Check type="checkbox" label="Check me out" />
            </Form.Group> */}
        <Button variant="primary" type="submit">
          Registrar Producto
        </Button>
      </Form>
    </Container>
  </Layout>
);

export default AddEmployee;
