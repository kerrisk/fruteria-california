import Layout from "../components/Layout";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import Container from "react-bootstrap/Container";

function handleSubmit(event) {
  console.log("Form submited");
  event.preventDefault();
  const data = new FormData(event.target);
  //console.log(event.target);
  console.log(...data);
  fetch("/addProduct", {
    method: "POST",
    body: data
  });
  handleSuccesfullySubmit();
}

function handleSuccesfullySubmit() {
  console.log("Submited");
}
const AddProduct = () => (
  <Layout>
    <Container>
      <div>
        <h1>Registro de nuevo producto</h1>
      </div>
      <Form
        onSubmit={event => handleSubmit(event)}
        encType="multipart/form-data"
      >
        <Form.Group controlId="prodName">
          <Form.Label>Nombre</Form.Label>
          <Form.Control
            type="text"
            placeholder="Ingrese Nombre del producto"
            name="prodName"
            required
          />
        </Form.Group>
        <Form.Group controlId="prodImage">
          <Form.Label>Seleccione una imagen del producto</Form.Label>
          <Form.Control
            type="file"
            name="prodImage"
            className="file"
            required
          />
        </Form.Group>
        <Form.Group controlId="prodDesc">
          <Form.Label>Descripcion</Form.Label>
          <Form.Control
            type="text"
            placeholder="Añadir una descripción del producto"
            name="prodDesc"
            required
          />
        </Form.Group>
        <Form.Group controlId="prodCost">
          <Form.Label>Costo Compra</Form.Label>
          <Form.Control
            type="number"
            placeholder="Ingrese el costo de compra del producto"
            name="prodCost"
            required
          />
        </Form.Group>
        <Form.Group controlId="prodPrice">
          <Form.Label>Precio Venta</Form.Label>
          <Form.Control
            type="number"
            placeholder="Ingrese el precio de venta del producto"
            name="prodPrice"
            required
          />
        </Form.Group>

        <Form.Group controlId="prodUnit">
          <Form.Label>Unidad de Venta</Form.Label>
          <Form.Control
            type="text"
            placeholder="Ingrese la unidad de venta del producto"
            name="prodUnit"
            required
          />
        </Form.Group>
        <Form.Group controlId="prodExis">
          <Form.Label>Existencia</Form.Label>
          <Form.Control
            type="number"
            placeholder="Ingrese la existencia del producto"
            name="prodExis"
            required
          />
        </Form.Group>
        <Form.Group controlId="prodReord">
          <Form.Label>Reordenar</Form.Label>
          <Form.Control
            type="number"
            placeholder="Ingrese el punto de Reorden"
            name="prodReord"
            required
          />
        </Form.Group>

        {/* <Form.Group controlId="formBasicCheckbox">
              <Form.Check type="checkbox" label="Check me out" />
            </Form.Group> */}
        <Button variant="primary" type="submit">
          Registrar Producto
        </Button>
      </Form>
    </Container>
  </Layout>
);

export default AddProduct;
