import Layout from "../components/Layout";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import Container from "react-bootstrap/Container";
import FormLabel from "react-bootstrap/FormLabel";
import FormControl from "react-bootstrap/FormControl";

function handleSubmit(event) {
  console.log("Form submited");
  event.preventDefault();
  const data = new FormData(event.target);
  //console.log(event.target);
  console.log(...data);
  fetch("/provRegistration", {
    method: "POST",
    body: data
  });
  handleSuccesfullySubmit();
}

function handleSuccesfullySubmit() {
  console.log("Submited");
}
const AddProv = () => (
  <Layout>
    <Container>
      <div>
        <h1>Registro de nuevo proveedor</h1>
      </div>
      <Form
        onSubmit={event => handleSubmit(event)}
        encType="multipart/form-data"
      >
        <Form.Group controlId="ProvName">
          <Form.Label>Nombre</Form.Label>
          <Form.Control
            type="text"
            placeholder="Ingrese Nombre del Proveedor"
            name="ProvName"
            required
          />
        </Form.Group>
        <Form.Group controlId="ProvEmail">
          <Form.Label>Correo Electrónico</Form.Label>
          <Form.Control
            type="email"
            placeholder="Ingrese correo electrónico del proveedor"
            name="ProvEmail"
            required
          />
        </Form.Group>
        <Form.Group controlId="ProvNumber">
          <Form.Label>Numero de Telefono Fijo</Form.Label>
          <Form.Control
            type="number"
            placeholder="Ingrese numero de Telefono Fijo del proveedor(10 dígitos)"
            name="ProvNumber"
            required
          />
        </Form.Group>
        <Form.Group controlId="ProvWhats">
          <Form.Label>WhatsApp del Proveedor</Form.Label>
          <Form.Control
            type="number"
            placeholder="Ingrese numero de contacto en Whatsapp del proveedor"
            name="ProvWhats"
            required
          />
        </Form.Group>
        <Form.Group controlId="ProvRFC">
          <Form.Label>RFC</Form.Label>
          <Form.Control
            type="text"
            placeholder="Ingrese RFC del Proveedor"
            name="ProvRFC"
            required
          />
        </Form.Group>
        {/* <Form.Group controlId="formBasicCheckbox">
              <Form.Check type="checkbox" label="Check me out" />
            </Form.Group> */}
        <Button variant="primary" type="submit">
          Registrar Proveedor
        </Button>
      </Form>
    </Container>
  </Layout>
);

export default AddProv;
