import Layout from "../../components/Admin/Layout";
import Link from "next/link";
import Button from "react-bootstrap/Button";
import Modal from "react-bootstrap/Modal";
import Form from "react-bootstrap/Form";
import Container from "react-bootstrap/Container";
import { useState, useContext } from "react";
import { Formik, Field } from "formik";
import UserContext from "../../components/UserContext";
import { MessageAlert } from "../../components/MessageAlert";

const Index = () => {
  const [showMsg, setShowMsg] = useState(false);
  const [MsgHead, setMsgHead] = useState("");
  const [MsgBody, setMsgBody] = useState("");
  const { signInAdmin } = useContext(UserContext);

  return (
    <Layout>
      <Container>
        <div>
          <h1>Login</h1>
        </div>
        <Formik
          initialValues={{ Username: "", Password: "" }}
          onSubmit={async (data, { setSubmitting }) => {
            setSubmitting(true);
            if (data.Username != "" || data.Password != "") {
              try {
                var loginRes = await signInAdmin(data.Username, data.Password);
              } catch (e) {
                //console.log("Login Res ");
                //console.log(e);
                if (e.Code != "200") {
                  setMsgHead(e.Code + " " + e.status);
                  setMsgBody(e.Description);
                  setShowMsg(true);
                  setSubmitting(false);
                }
              }
            }
          }}
        >
          {({ values, isSubmitting, handleSubmit }) => (
            <>
              <MessageAlert
                f={{ showMsg: showMsg, setShowMsg: setShowMsg }}
                Heading={MsgHead}
                Body={MsgBody}
                variant="danger"
              />

              <Form onSubmit={handleSubmit} encType="multipart/form-data">
                <Form.Group controlId="Username">
                  <Form.Label>Usuario</Form.Label>
                  <Field
                    as={Form.Control}
                    type="text"
                    placeholder="Ingresa tu usuario"
                    name="Username"
                    required
                  />
                </Form.Group>
                <Form.Group controlId="Password">
                  <Form.Label>Contraseña</Form.Label>
                  <Field
                    as={Form.Control}
                    type="password"
                    placeholder="Ingresa tu usuario"
                    name="Password"
                    required
                  />
                </Form.Group>
                <Button variant="primary" type="submit" disabled={isSubmitting}>
                  Submit
                </Button>
                <br></br>
                <pre>{JSON.stringify(values, null, 2)}</pre>
              </Form>
            </>
          )}
        </Formik>
      </Container>
    </Layout>
  );
};

export default Index;
