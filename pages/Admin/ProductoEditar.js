import Layout from "../../components/Admin/Layout";
import { withRouter } from "next/router";
import { useState, useContext, useEffect } from "react";
import Row from "react-bootstrap/Row";
import Link from "next/link";
import ProductoEditarForm from "../../components/Admin/ProductoEditarForm";
import Container from "react-bootstrap/Container";

const ProductoEditar = props => {
  const [fields, setFields] = useState({});
  const onChange = updatedValue => {
    this.setState({
      fields: {
        ...fields,
        ...updatedValue
      }
    });
  };
  return (
    <Layout>
      {props.router.query.msg !== undefined ? <Row></Row> : ""}
      <Row>
        <div className="mx-auto">
          <h1>Editar Producto</h1>
        </div>
      </Row>
      <Row>
        <Container fluid>
          <ProductoEditarForm id_producto={props.router.query.id_producto} />
        </Container>
      </Row>
    </Layout>
  );
};

export default withRouter(ProductoEditar);
