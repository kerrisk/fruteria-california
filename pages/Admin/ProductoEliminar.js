import Layout from "../../components/Admin/Layout";
import { withRouter } from "next/router";
import ToastRight from "../../components/ToastRight";
import Col from "react-bootstrap/Col";
import { useState, useContext } from "react";
import Row from "react-bootstrap/Row";
import Router from "next/router";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import { MessageAlert } from "../../components/MessageAlert";

const ProductoEliminar = props => {
  //if (props.router.query.msg !== undefined) {
  const [showMsg, setShowMsg] = useState(props.router.query.msg);
  //}
  const [MsgHead, setMsgHead] = useState("");
  const [MsgBody, setMsgBody] = useState("");
  const [MsgVariant, setMsgVariant] = useState("");
  const [isSubmitting, setSubmitting] = useState(false);
  function handleSubmit(event) {
    event.preventDefault();
    setSubmitting(true);
    const data = new FormData(event.target);
    console.log(event.target);
    console.log(...data);
    fetch("/api/ProductoRemove", {
      method: "POST",
      body: data
    })
      .then(resp => resp.json())
      .then(function(data) {
        if (data.status !== "OK") {
          setMsgHead(data.Code + " " + data.status);
          setMsgBody(data.Description);
          setShowMsg(true);
          setMsgVariant("danger");
        } else {
          //document.getElementById("clienteRegistroForm").reset();
          Router.push({
            pathname: "/Admin/Productos",
            query: {
              msg: true,
              MsgHead: "Exito",
              MsgBody: "Producto Eliminado correctamente",
              MsgVariant: "primary"
            }
          });
        }
        //console.log(data.status);
      })
      .catch(err => {
        console.error(err);
      })
      .finally(final => {
        setSubmitting(false);
      });
  }
  return (
    <Layout>
      <MessageAlert
        f={{ showMsg: showMsg, setShowMsg: setShowMsg }}
        Heading={MsgHead}
        Body={MsgBody}
        variant={MsgVariant}
      />

      <Row>
        <div className="mx-auto">
          <h1>Eliminar Producto</h1>
        </div>
      </Row>

      <Form
        onSubmit={event => handleSubmit(event)}
        encType="multipart/form-data"
        method="POST"
        id="clienteRegistroForm"
        noValidate
      >
        <Form.Group
          as={Col}
          xs={12}
          md={8}
          className="mx-auto"
          controlId="id_producto"
        >
          <Form.Label>Id producto</Form.Label>
          <Form.Control
            type="text"
            placeholder="Ingrese del id_producto del Producto"
            name="id_producto"
            value={props.router.query.id_producto}
            readOnly
            required
          />
        </Form.Group>
        <Button
          variant="primary"
          disabled={isSubmitting}
          type="submit"
          className="mx-auto"
        >
          Eliminar
        </Button>
      </Form>
    </Layout>
  );
};

export default withRouter(ProductoEliminar);
