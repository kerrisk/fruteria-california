import Layout from "../../components/Admin/Layout";
import { withRouter } from "next/router";
import { MessageAlert } from "../../components/MessageAlert";
import ToastRight from "../../components/ToastRight";
import { useState, useContext } from "react";
import Row from "react-bootstrap/Row";
import UserContext from "../../components/UserContext";
import Productos from "../../components/Admin/Productos";

const Index = props => {
  //if (props.router.query.msg !== undefined) {
  const [showMsg, setShowMsg] = useState(props.router.query.msg);
  //}
  const { id_empleado } = useContext(UserContext);
  /*   if (id_empleado) {
    Router.push("/Admin/Productos");
  }
 */
  const [MsgHead, setMsgHead] = useState("");
  const [MsgBody, setMsgBody] = useState("");
  const [MsgVariant, setMsgVariant] = useState("");
  return (
    <Layout>
      {props.router.query.msg !== undefined ? (
        <Row>
          <ToastRight
            f={{ showMsg: showMsg, setShowMsg: setShowMsg }}
            Heading={props.router.query.MsgHead}
            Body={props.router.query.MsgBody}
            variant={props.router.query.MsgVariant}
          />
        </Row>
      ) : (
        ""
      )}
      <Row>
        <div className="mx-auto">
          <h1>Productos</h1>
        </div>
        <Productos />
      </Row>
    </Layout>
  );
};

export default withRouter(Index);
