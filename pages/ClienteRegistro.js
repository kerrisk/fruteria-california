import Layout from "../components/Layout";
import { useState, useContext } from "react";
import { MessageAlert } from "../components/MessageAlert";
import Container from "react-bootstrap/Container";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import Col from "react-bootstrap/Col";
import Router from "next/router";

function handleSubmit(event, setSubmitting, f) {
  event.preventDefault();
  setSubmitting(true);
  const data = new FormData(event.target);
  //console.log(event.target);
  //console.log(...data);
  fetch("/api/ClienteRegistro", {
    method: "POST",
    body: data
  })
    .then(resp => resp.json())
    .then(function(data) {
      if (data.status !== "OK") {
        f.setMsgHead(e.Code + " " + e.status);
        f.setMsgBody(e.Description);
        f.setShowMsg(true);
        f.setMsgVariant("danger");
      } else {
        document.getElementById("clienteRegistroForm").reset();
      }
      //console.log(data.status);
      Router.push({
        pathname: "/",
        query: {
          msg: true,
          MsgHead: "Exito",
          MsgBody: "Usuario registrado en la base de datos",
          MsgVariant: "primary"
        }
      });
    })
    .finally(f => {
      setSubmitting(false);
    });
}
function handleSuccesfullySubmit() {
  console.log("Submited");
}
const UserReg = () => {
  const [showMsg, setShowMsg] = useState(false);
  const [MsgHead, setMsgHead] = useState("");
  const [MsgBody, setMsgBody] = useState("");
  const [MsgVariant, setMsgVariant] = useState("");
  const [isSubmitting, setSubmitting] = useState(false);
  return (
    <Layout>
      <Container>
        <div>
          <h1>Registro de nuevo cliente</h1>
        </div>
        <MessageAlert
          f={{ showMsg: showMsg, setShowMsg: setShowMsg }}
          Heading={MsgHead}
          Body={MsgBody}
          variant={MsgVariant}
        />
        <Form
          onSubmit={event =>
            handleSubmit(event, setSubmitting, {
              setMsgHead: setMsgHead,
              setMsgBody: setMsgBody,
              setShowMsg: setShowMsg,
              setMsgVariant: setMsgVariant
            })
          }
          encType="multipart/form-data"
          method="POST"
          id="clienteRegistroForm"
          noValidate
        >
          <Form.Row>
            <Form.Group as={Col} xs={12} md={8} controlId="Nombre">
              <Form.Label>Nombre</Form.Label>
              <Form.Control
                type="text"
                placeholder="Ingrese su Nombre"
                name="Nombre"
                required
              />
            </Form.Group>
            <Form.Group as={Col} xs={12} md={4} controlId="avatar">
              <Form.Label>Ingrese imagen de Perfil</Form.Label>
              <Form.Control
                type="file"
                name="avatar"
                className="file"
                required
              />
            </Form.Group>
          </Form.Row>
          <br />
          <Form.Row>
            <Form.Group as={Col} xs={12} md={4} controlId="Email">
              <Form.Label>Correo Electrónico</Form.Label>
              <Form.Control
                type="email"
                placeholder="Ingrese su correo electrónico"
                name="Email"
                required
              />
            </Form.Group>
            <Form.Group as={Col} xs={12} md={4} controlId="Telefono">
              <Form.Label>Numero de Telefono</Form.Label>
              <Form.Control
                type="number"
                placeholder="Ingrese su numero de Telefono (10 dígitos)"
                name="Telefono"
                required
              />
            </Form.Group>
            <Form.Group as={Col} xs={12} md={4} controlId="RFC">
              <Form.Label>RFC</Form.Label>
              <Form.Control
                type="text"
                placeholder="Ingrese su RFC"
                maxLength="13"
                name="RFC"
                required
              />
            </Form.Group>
          </Form.Row>
          <br />
          <Form.Row>
            <Form.Group as={Col} xs={12} md={6} controlId="Password">
              <Form.Label>Contraseña</Form.Label>
              <Form.Control
                type="password"
                placeholder="Ingrese su Contraseña"
                name="Password"
                required
              />
            </Form.Group>
            <Form.Group as={Col} xs={12} md={4} controlId="PasswordCheck">
              <Form.Label>Repita la Contraseña</Form.Label>
              <Form.Control
                type="password"
                placeholder="Ingrese de nuevo su Contraseña"
                name="PasswordCheck"
                required
              />
            </Form.Group>
          </Form.Row>
          <Button
            disabled={isSubmitting}
            variant="primary"
            type="submit"
            className="float-right"
          >
            Registrar
          </Button>
        </Form>
      </Container>
    </Layout>
  );
};

export default UserReg;
