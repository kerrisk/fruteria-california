import Layout from "../components/Layout";
import Row from "react-bootstrap/Row";
import Nav from "react-bootstrap/Nav";
import Container from "react-bootstrap/Container";
import Col from "react-bootstrap/Col";

const Index = () => {
  return (
    <Layout>
      <Container variant="pills" fluid>
        <Row>
          <Col>
            <Nav defaultActiveKey="/home" className="flex-column">
              <Nav.Link href="/">Active</Nav.Link>
              <Nav.Link eventKey="link-1">Link</Nav.Link>
              <Nav.Link eventKey="link-2">Link</Nav.Link>
              <Nav.Link eventKey="disabled" disabled>
                Disabled
              </Nav.Link>
            </Nav>
          </Col>
          <Col>
            <h1>{"Página de consultas"}</h1>
          </Col>
        </Row>
      </Container>
    </Layout>
  );
};

export default Index;
