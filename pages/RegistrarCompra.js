import Layout from "../components/Layout";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import Container from "react-bootstrap/Container";

function handleSubmit(event) {
  console.log("Form submited");
  event.preventDefault();
  const data = new FormData(event.target);
  //console.log(event.target);
  console.log(...data);
  fetch("/profile", {
    method: "POST",
    body: data
  });
  handleSuccesfullySubmit();
}

function handleSuccesfullySubmit() {
  console.log("Submited");
}
const RegistrarCompra = () => (
  <Layout>
    <Container>
      <div>
        <h1>Registro de compra a Proveedor</h1>
      </div>
      <Form
        onSubmit={event => handleSubmit(event)}
        encType="multipart/form-data"
      >
        <Form.Group controlId="registerName">
          <Form.Label>Nombre</Form.Label>
          <Form.Control
            type="select"
            placeholder="Ingrese su Nombre"
            name="registerName"
            required
          />
        </Form.Group>
        <Form.Group controlId="registerName">
          <Form.Label>Ingrese imagen suya</Form.Label>
          <Form.Control
            type="file"
            name="registerImage"
            className="file"
            required
          />
        </Form.Group>
        <Form.Group controlId="registerEmail">
          <Form.Label>Correo Electrónico</Form.Label>
          <Form.Control
            type="text"
            placeholder="Ingrese su correo electrónico"
            name="registerEmail"
            required
          />
        </Form.Group>
        <Form.Group controlId="registerNumber">
          <Form.Label>Numero de Telefono</Form.Label>
          <Form.Control
            type="number"
            placeholder="Ingrese su numero de Telefono (10 dígitos)"
            name="registerNumber"
            required
          />
        </Form.Group>
        <Form.Group controlId="registerPassword">
          <Form.Label>Contraseña</Form.Label>
          <Form.Control
            type="password"
            placeholder="Ingrese su Contraseña"
            name="registerPassword"
            required
          />
        </Form.Group>
        <Form.Group controlId="registerPasswordCheck">
          <Form.Label>Repita la Contraseña</Form.Label>
          <Form.Control
            type="password"
            placeholder="Ingrese de nuevo su Contraseña"
            name="registerPasswordCheck"
            required
          />
        </Form.Group>
        {/* <Form.Group controlId="formBasicCheckbox">
              <Form.Check type="checkbox" label="Check me out" />
            </Form.Group> */}
        <Button variant="primary" type="submit">
          Registrar
        </Button>
      </Form>
    </Container>
  </Layout>
);

export default RegistrarCompra;
