import React from "react";
import App from "next/app";
import Router from "next/router";
import UserContext from "../components/UserContext";
import fetch from "isomorphic-unfetch";
import { throws } from "assert";

export default class MyApp extends App {
  state = {
    cliente: {
      id_cliente: null,
      Email: null,
      Nombre: null,
      Telefono: null,
      RFC: null
    },
    admin: {
      id_empleado: null,
      Nombre: null,
      Puesto: null,
      Email: null,
      id_Jefe: null,
      Telefono: null,
      Username: null
    }
  };

  componentDidMount = () => {
    var newState = {};
    var clienteInfo = Object.entries(this.state.cliente);

    clienteInfo.map(function([key, value], index) {
      value = localStorage.getItem(key);
      newState[key] = value;
    });
    //console.log(newState);
    if (newState["id_cliente"]) {
      this.setState({ ...this.state, cliente: newState });
      return;
    }
    newState = {};
    var clienteInfo = Object.entries(this.state.admin);

    clienteInfo.map(function([key, value], index) {
      value = localStorage.getItem(key);
      newState[key] = value;
    });
    if (newState["id_empleado"]) {
      this.setState({ ...this.state, admin: newState });
    }
  };

  signIn = async (Email, Password) => {
    try {
      var respServer = new Promise(function(resolve, reject) {
        fetch("/api/ClienteLogin", {
          method: "POST",
          headers: {
            "Content-Type": "application/json"
          },
          body: JSON.stringify({
            Email: Email,
            Password: Password
          })
        })
          .then(resp => resp.json())
          .then(function(data) {
            //console.log(data.status);
            if (data.status == "OK") {
              //console.log(data);

              resolve(data);
            } else {
              console.log("Login failed Fetch");
              reject(data);
            }
          })
          .catch(err => {
            console.log("Login failed Fetch catch");
            console.error(err);
            reject(err);
          });
      });
      respServer
        .then(resp => {
          console.log("awaited?");
          console.log(resp);
          console.log("awaited?");
          resp.result.forEach(element => {
            var clienteInfo = Object.entries(element);
            clienteInfo.map(function([key, value], index) {
              //console.log(key + " " + value);
              localStorage.setItem(key, value);
            });
          });
          this.setState({ ...this.state, cliente: resp.result[0] }, () => {
            Router.push("/");
          });
        })
        .catch(err => {
          if (err.Code == "404") {
            console.log("Login Failed 404");
            return err;
          }
        });

      return respServer;
    } catch (e) {
      console.log("Error");
      console.error(e);
    }
  };

  signOut = () => {
    fetch("/api/ClienteLogout", { method: "POST" })
      .then(res => res.json())
      .then(data => {
        console.log("SignOut Succesful");
        console.log(data);
      })
      .catch(err => console.log(err));
    var clienteInfo = Object.entries(this.state.cliente);
    clienteInfo.map(function([key, value], index) {
      localStorage.removeItem(key);
    });
    this.setState({
      ...this.state,
      cliente: {
        id_cliente: null,
        Email: null,
        Nombre: null,
        Telefono: null,
        RFC: null
      }
    });
    Router.push("/");
  };

  render() {
    const { Component, pageProps } = this.props;
    var stateInUse = this.state.cliente;
    //console.log("id_Cliente: " + stateInUse.id_cliente);
    if (this.state.admin.id_empleado !== null) {
      stateInUse = this.state.admin;
    }
    //console.log(stateInUse);
    return (
      <UserContext.Provider
        value={{
          ...stateInUse,
          signIn: this.signIn,
          signOut: this.signOut,
          signInAdmin: this.signInAdmin,
          signOutAdmin: this.signOutAdmin
        }}
      >
        <Component {...pageProps} />
      </UserContext.Provider>
    );
  }

  signInAdmin = async (Username, Password) => {
    try {
      var respServer = new Promise(function(resolve, reject) {
        fetch("/api/Admin/AdminLogin", {
          method: "POST",
          headers: {
            "Content-Type": "application/json"
          },
          body: JSON.stringify({
            Username: Username,
            Password: Password
          })
        })
          .then(resp => resp.json())
          .then(function(data) {
            //console.log(data.status);
            if (data.status == "OK") {
              //console.log(data);

              resolve(data);
            } else {
              console.log("Login failed Fetch");
              reject(data);
            }
          })
          .catch(err => {
            console.log("Login failed Fetch catch");
            console.log(err);
            reject(err);
          });
      });
      respServer
        .then(resp => {
          console.log("awaited AdminLogin?");
          console.log(resp);
          console.log("awaited AdminLogin?");
          resp.result.forEach(element => {
            var adminInfo = Object.entries(element);
            adminInfo.map(function([key, value], index) {
              console.log(key + " " + value);
              localStorage.setItem(key, value);
            });
          });
          this.setState({ ...this.state, admin: resp.result[0] }, () => {
            Router.push("/Admin/index");
          });
        })
        .catch(err => {
          if (err.Code == "404") {
            console.log("Login Failed 404");
            return err;
          }
        });

      return respServer;
    } catch (e) {
      console.log("Error");
      console.error(e);
    }
  };
  signOutAdmin = () => {
    fetch("/api/Admin/AdminLogout", { method: "POST" })
      .then(res => res.json())
      .then(data => {
        console.log(data);
      })
      .catch(err => console.log(err));
    var adminInfo = Object.entries(this.state.admin);
    adminInfo.map(function([key, value], index) {
      localStorage.removeItem(key);
    });
    this.setState({
      ...this.state,
      admin: {
        id_empleado: null,
        Nombre: null,
        Puesto: null,
        Email: null,
        id_Jefe: null,
        Telefono: null,
        Username: null
      }
    });
    Router.push("/Admin/Login");
  };
}
