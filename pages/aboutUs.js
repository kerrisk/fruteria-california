import Layout from "../components/Layout";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";

const Index = () => {
  return (
    <Layout>
      <div>
        <h1>About Us!</h1>
      </div>
      <Row>
        <Col>
          <div className="mapouter ">
            <div className="gmap_canvas">
              <iframe
                width="1000"
                height="700"
                id="gmap_canvas"
                src="https://maps.google.com/maps?q=Arnulfo%20M.%20Vald%C3%A9z%2026%2C%20Centro%20Comercial%20Agropecuario%2C%2020135%20Aguascalientes%2C%20Ags.&t=&z=15&ie=UTF8&iwloc=&output=embed"
                frameborder="0"
                scrolling="no"
                marginHeight="0"
                marginWidth="0"
              ></iframe>
            </div>
          </div>
        </Col>
        <Col>
          <h2>Somos una empresa familiar</h2>
          <p>
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Ullam hic,
            officiis itaque dicta molestias repellendus sint saepe repudiandae
            dignissimos assumenda, accusantium similique cupiditate est enim,
            earum commodi sunt suscipit aspernatur.
          </p>
        </Col>
      </Row>
    </Layout>
  );
};

export default Index;
