import Layout from "../components/Layout";
import Productos from "../components/Productos";

const Index = () => {
  return (
    <Layout>
      <div>
        <h1>Frutas!</h1>
        <Productos id_productoCategoria="1" />
      </div>
    </Layout>
  );
};

export default Index;
