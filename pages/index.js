import Layout from "../components/Layout";
import { withRouter } from "next/router";
import { MessageAlert } from "../components/MessageAlert";
import ToastRight from "../components/ToastRight";
import { useState, useContext } from "react";
import Row from "react-bootstrap/Row";
import Carusel from "../components/Carusel";
import Jumbotron from "react-bootstrap/Jumbotron";
import Button from "react-bootstrap/Button";
import Col from "react-bootstrap/Col";
import Link from "next/link";

const Index = props => {
  //if (props.router.query.msg !== undefined) {
  const [showMsg, setShowMsg] = useState(props.router.query.msg);
  //}
  const [MsgHead, setMsgHead] = useState("");
  const [MsgBody, setMsgBody] = useState("");
  const [MsgVariant, setMsgVariant] = useState("");
  return (
    <Layout>
      {props.router.query.msg !== undefined ? (
        <ToastRight
          f={{ showMsg: showMsg, setShowMsg: setShowMsg }}
          Heading={props.router.query.MsgHead}
          Body={props.router.query.MsgBody}
          variant={props.router.query.MsgVariant}
        />
      ) : (
        ""
      )}
      <Row>
        <Jumbotron as={Col} xs={12} md={8} className="mx-auto">
          <h1>Bienvenido!</h1>
          <p>A Fruteria California</p>
          <Row>
            <Col>
              <Link href="/frutas">
                <Button variant="primary">Frutas</Button>
              </Link>
            </Col>
            <Col>
              <Link href="/verduras">
                <Button variant="secondary">Verduras</Button>
              </Link>
            </Col>
            <Col>
              <Link href="/quesos">
                <Button variant="warning">Quesos</Button>
              </Link>
            </Col>
            <Col>
              <Link href="/semillas">
                <Button variant="success">Semillas & Cereales</Button>
              </Link>
            </Col>
          </Row>
        </Jumbotron>
      </Row>
      <Row>
        <Carusel />
      </Row>
    </Layout>
  );
};

export default withRouter(Index);
