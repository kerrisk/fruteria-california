import Layout from "../components/Layout";
import Productos from "../components/Productos";

const Index = () => {
  return (
    <Layout>
      <div>
        <h1>Quesos!</h1>
        <Productos id_productoCategoria="3" />
      </div>
    </Layout>
  );
};

export default Index;
