import Layout from "../components/Layout";
import Productos from "../components/Productos";

const Index = () => {
  return (
    <Layout>
      <div>
        <h1>Semillas!</h1>
        <Productos id_productoCategoria="4" />
      </div>
    </Layout>
  );
};

export default Index;
