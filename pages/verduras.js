import Layout from "../components/Layout";
import Productos from "../components/Productos";

const Index = () => {
  return (
    <Layout>
      <div>
        <h1>Verduras!</h1>
        <Productos id_productoCategoria="2" />
      </div>
    </Layout>
  );
};

export default Index;
