var express = require("express");
var router = express.Router();
var multer = require("multer");
var upload = multer({ dest: "./public/uploads/" });
var con = require("../databaseConnection");

router.use(function(req, res, next) {
  // log each request to the console
  console.log("--------------------------------");
  console.log(req.method, req.url);
  // continue doing what we were doing and go to the route
  next();
});
router.post("/AdminLogin", upload.none(), (req, res) => {
  const requiredParameters = {
    Username: req.body.Username,
    Password: req.body.Password
  };
  adminQuery(
    "SELECT id_empleado, Nombre, Puesto, Email, id_Jefe, Telefono, Username FROM empleado WHERE Username=? AND Password=?;",
    requiredParameters,
    "Usuario/Contraseña incorrectos"
  )
    .then(value => {
      Object.entries(value.result[0]).map(function([key, value], index) {
        req.session[key] = value;
      });
      req.session["tipo"] = 1;
      res.json(value);
    })
    .catch(err => {
      console.error(err);
      res.json(err);
    });
  console.log(req.body);
});
router.post("/AdminLogout", upload.none(), (req, res) => {
  req.session.destroy(function(err) {
    res.json({
      Code: "200",
      status: "OK",
      Description: `User session destroyed`
    });
  });
});
function adminQuery(queryString, requiredParameters, noResultMsg) {
  var adminQueryPromise = new Promise(function(resolve, reject) {
    const requiredFields = [];
    //console.log(requiredParameters);
    for (const property in requiredParameters) {
      if (typeof requiredParameters[property] === "undefined") {
        reject({
          Code: "400",
          status: "Bad Request",
          Description: `The request was invalid. Missing ${property} value`,
          RequiredParameters: requiredParameters
        });
      }
      requiredFields.push(requiredParameters[property]);
    }
    con.query(queryString, requiredFields, function(error, results) {
      if (error) {
        console.error("Error while querying the database:  " + error);
        console.error(error.sql);
        reject({
          Code: "500",
          status: "Internal Server Error",
          Description:
            "The request was not completed due to an internal error on the server side."
        });
        //throw error;
      }
      //console.log(results);
      if (results.length > 0) {
        resolve({
          Code: "200",
          status: "OK",
          result: results,
          length: results.length,
          Description: `Number of returned rows ${results.length}`
        });
      } else if (results.affectedRows) {
        resolve({
          Code: "200",
          status: "OK",
          result: results,
          affectedRows: results.affectedRows,
          Description: `Number of affected rows ${results.affectedRows}`
        });
      } else {
        reject({
          Code: "404",
          status: "Not Found",
          Description: noResultMsg
        });
      }
    });
  });
  return adminQueryPromise;
}
module.exports = router;
