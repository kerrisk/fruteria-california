var express = require("express");
var router = express.Router();
var multer = require("multer");
var upload = multer({ dest: "./public/uploads" });
var con = require("../databaseConnection");

router.use(function(req, res, next) {
  // log each request to the console
  console.log("--------------------------------");
  console.log(req.method, req.url);
  // continue doing what we were doing and go to the route
  next();
});

router.post("/ClienteLogin", upload.none(), (req, res) => {
  const requiredParameters = {
    Email: req.body.Email,
    Password: req.body.Password
  };
  clienteQuery(
    "SELECT id_cliente, Nombre, Email, Telefono, RFC FROM cliente WHERE Email=? AND Password=?;",
    requiredParameters,
    "Usuario/Contraseña incorrectos"
  )
    .then(value => {
      Object.entries(value.result[0]).map(function([key, value], index) {
        req.session[key] = value;
      });
      req.session["tipo"] = 0;
      res.json(value);
    })
    .catch(err => {
      console.error(err);
      res.json(err);
    });
  //console.log(req.body);
});
router.post("/ClienteLogout", upload.none(), (req, res) => {
  req.session.destroy(function(err) {
    res.json({
      Code: "200",
      status: "OK",
      Description: `User session destroyed`
    });
  });
});
router.post("/ClienteConsulta", upload.none(), (req, res) => {
  const requiredParameters = {
    id_cliente: req.body.id_cliente
  };
  clienteQuery(
    "SELECT * FROM cliente WHERE id_cliente = ?;",
    requiredParameters
  )
    .then(value => {
      res.json(value);
    })
    .catch(err => {
      console.error(err);
      res.json(err);
    });
});
router.post("/ClienteRegistro", upload.single("avatar"), (req, res) => {
  const requiredParameters = {
    Nombre: req.body.Nombre,
    Email: req.body.Email,
    Password: req.body.Password,
    Telefono: req.body.Telefono,
    RFC: req.body.RFC || "", //Verifies that RFC is not defined if it is not it send an empty string to the database
    ruta: "uploads/" + req.file.filename
  };
  clienteQuery(
    "INSERT INTO cliente VALUES (0,?,?,?,?,?,?);",
    requiredParameters,
    "Unable to insert cliente into database"
  )
    .then(value => {
      console.log(value);
      console.log(value.result.insertId);

      /* if (req.files.length > 0) {
        console.log(req.files);
        for (const picture in req.files) {
          console.log(picture);
          clienteQuery(
            "INSERT INTO clienteimagen VALUES(0,?,?)",
            {
              id_cliente: value.result.insertId,
              ruta: "uploads/" + req.files[picture].filename
            },
            "Unable to insert clienteimagen into database"
          ).catch(err => {
            console.error(err);
            res.json(err);
          });
        }
      } */
      res.json(value);
    })
    .catch(err => {
      console.error(err);
      res.json(err);
    });
});
function clienteQuery(queryString, requiredParameters, noResultMsg) {
  var clienteQueryPromise = new Promise(function(resolve, reject) {
    const requiredFields = [];
    //console.log(requiredParameters);
    for (const property in requiredParameters) {
      if (typeof requiredParameters[property] === "undefined") {
        reject({
          Code: "400",
          status: "Bad Request",
          Description: `The request was invalid. Missing ${property} value`,
          RequiredParameters: requiredParameters
        });
      }
      requiredFields.push(requiredParameters[property]);
    }
    con.query(queryString, requiredFields, function(error, results) {
      if (error) {
        console.error("Error while querying the database:  " + error);
        console.error(error.sql);
        reject({
          Code: "500",
          status: "Internal Server Error",
          Description:
            "The request was not completed due to an internal error on the server side."
        });
        //throw error;
      }
      //console.log(results);
      if (results.length > 0) {
        resolve({
          Code: "200",
          status: "OK",
          result: results,
          length: results.length,
          Description: `Number of returned rows ${results.length}`
        });
      } else if (results.affectedRows) {
        resolve({
          Code: "200",
          status: "OK",
          result: results,
          affectedRows: results.affectedRows,
          Description: `Number of affected rows ${results.affectedRows}`
        });
      } else {
        reject({
          Code: "404",
          status: "Not Found",
          Description: noResultMsg
        });
      }
    });
  });
  return clienteQueryPromise;
}
module.exports = router;
