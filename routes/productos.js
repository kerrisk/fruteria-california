var express = require("express");
var router = express.Router();
var multer = require("multer");
var upload = multer({ dest: "./public/uploads/" });
var con = require("../databaseConnection");

router.use(function(req, res, next) {
  // log each request to the console
  console.log("--------------------------------");
  console.log(req.method, req.url);
  // continue doing what we were doing and go to the route
  next();
});
router.post("/ProductosConsulta", upload.none(), (req, res) => {
  const requiredParameters = {};
  productoQuery("SELECT * FROM producto;", requiredParameters)
    .then(value => {
      res.json(value);
    })
    .catch(err => {
      console.error(err);
      res.json(err);
    });
});
router.post("/ProductosConsultaCat", upload.none(), (req, res) => {
  console.log(req.body);
  const requiredParameters = {
    id_productoCategoria: req.body.id_productoCategoria
  };
  console.log(requiredParameters);
  productoQuery(
    "SELECT * FROM producto WHERE id_productoCategoria=?;",
    requiredParameters
  )
    .then(value => {
      res.json(value);
    })
    .catch(err => {
      console.error(err);
      res.json(err);
    });
});
router.post("/ProductoConsulta", upload.none(), (req, res) => {
  const requiredParameters = { id_producto: req.body.id_producto };
  productoQuery(
    "SELECT * FROM producto WHERE id_producto=?;",
    requiredParameters
  )
    .then(value => {
      res.json(value);
    })
    .catch(err => {
      console.error(err);
      res.json(err);
    });
});
router.post("/ProductoUpdate", upload.none(), (req, res) => {
  const requiredParameters = {
    Descripcion: req.body.Descripcion,
    CostoCompra: req.body.CostoCompra,
    CostoVenta: req.body.CostoVenta,
    id_productoCategoria: req.body.id_productoCategoria,
    Existencia: req.body.Existencia,
    PuntoReorden: req.body.PuntoReorden,
    UnidadMedida: req.body.UnidadMedida,
    Nombre: req.body.Nombre,
    ruta: req.body.ruta,
    id_producto: req.body.id_producto
  };
  productoQuery(
    "UPDATE producto SET Descripcion=?,CostoCompra=?,CostoVenta=?,id_productoCategoria=?,Existencia=?,\
    PuntoReorden=?,UnidadMedida=?,Nombre=?,ruta=? WHERE id_producto=?;",
    requiredParameters
  )
    .then(value => {
      res.json(value);
    })
    .catch(err => {
      console.error(err);
      res.json(err);
    });
});
router.post("/ProductoRemove", upload.none(), (req, res) => {
  const requiredParameters = { id_producto: req.body.id_producto };
  productoQuery("DELETE FROM producto WHERE id_producto=?;", requiredParameters)
    .then(value => {
      res.json(value);
    })
    .catch(err => {
      console.error(err);
      res.json(err);
    });
});
function productoQuery(queryString, requiredParameters, noResultMsg) {
  var productoQueryPromise = new Promise(function(resolve, reject) {
    const requiredFields = [];
    //console.log(requiredParameters);
    for (const property in requiredParameters) {
      if (typeof requiredParameters[property] === "undefined") {
        reject({
          Code: "400",
          status: "Bad Request",
          Description: `The request was invalid. Missing ${property} value`,
          RequiredParameters: requiredParameters
        });
      }
      requiredFields.push(requiredParameters[property]);
    }
    con.query(queryString, requiredFields, function(error, results) {
      if (error) {
        console.error("Error while querying the database:  " + error);
        console.error(error.sql);
        reject({
          Code: "500",
          status: "Internal Server Error",
          Description:
            "The request was not completed due to an internal error on the server side."
        });
        //throw error;
      }
      //console.log(results);
      if (results.length > 0) {
        resolve({
          Code: "200",
          status: "OK",
          result: results,
          length: results.length,
          Description: `Number of returned rows ${results.length}`
        });
      } else if (results.affectedRows) {
        resolve({
          Code: "200",
          status: "OK",
          result: results,
          affectedRows: results.affectedRows,
          Description: `Number of affected rows ${results.affectedRows}`
        });
      } else {
        reject({
          Code: "404",
          status: "Not Found",
          Description: noResultMsg
        });
      }
    });
  });
  return productoQueryPromise;
}
module.exports = router;
