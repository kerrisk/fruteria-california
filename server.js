var express = require("express");
const next = require("next");
var session = require("express-session");
var debug = require("debug");
var crypto = require("crypto");
var nodemailer = require("nodemailer");
var con = require("./databaseConnection");
var multer = require("multer");
var upload = multer();
const health = require("@cloudnative/health-connect");
let healthcheck = new health.HealthChecker();
var prometheus = require("appmetrics-prometheus").attach();

const port = parseInt(process.env.PORT, 10) || 3333;
const dev = process.env.NODE_ENV !== "production";
const app = next({
  dev
});
const handle = app.getRequestHandler();
// Test Database Connection
con.getConnection(function(err, connection) {
  if (err) {
    if (err.fatal) {
      console.error("Fatal error when connecting to database... " + err);
      throw err;
    }
    console.error("Non Fatal error " + err);
  } // not connected!
  // When done with the connection, release it.
  connection.release();
});

app
  .prepare()
  .then(() => {
    const server = express();
    //Recupera los valores enviados por POST
    server.use(
      express.urlencoded({
        extended: true
      })
    );
    server.use(express.json());
    server.use(express.static("public"));
    server.use(
      session({
        name: "qid",
        secret: "keyboard american stanford",
        resave: false,
        saveUninitialized: false,
        cookie: {
          httpOnly: true,
          secure: false,
          maxAge: 1000 * 60 * 60 * 24 * 7 // 7 dias
        }
      })
    );
    //Routes
    server.use("/api", require("./routes/client"));
    server.use("/api", require("./routes/productos"));
    server.use("/health", health.LivenessEndpoint(healthcheck));
    function isAuthenticated(req, res, next) {
      // do any checks you want to in here

      // CHECK THE USER STORED IN SESSION FOR A CUSTOM VARIABLE
      // you can do this however you want with whatever variables you set up
      console.log("--------------Admin API------------------");
      console.log(req.method, req.url);

      if (req.url == "/AdminLogin" || req.session.tipo) {
        //console.log("Admin Login approved");
        return next();
      }
      //console.log(req.session);
      // IF A USER ISN'T LOGGED IN, THEN REDIRECT THEM SOMEWHERE
      res.json({
        Code: "401",
        status: "Unauthorized",
        Description:
          "The request did not include an authentication token or the authentication token was expired."
      });
    }
    /* server.use("/Admin/", function(req, res, next) {
      // log each request to the console
      console.log("--------------Admin Pages------------------");
      console.log(req.method, req.url);
      //console.log(req.session.tipo);
      if (req.session.tipo) {
        //res.redirect("/Admin/Login");
        return next;
      }
      return app.render(req, res, "/Admin/Login");
      // continue doing what we were doing and go to the route
      next();
    }); */
    server.use("/api/Admin", /* isAuthenticated,*/ require("./routes/admin"));
    /*server.use(function(req, res, next) {
      // log each request to the console
      console.log("--------------------------------");
      console.log(req.method, req.url);
      console.log(req.session);
      // continue doing what we were doing and go to the route
      next();
    });*/
    /* server.use(upload.none(), function(req, res, next) {
      console.log(req.body); // populated!
      next();
    }); */

    server.post("/api/provRegistration", upload.none(), (req, res) => {
      con.query(
        "INSERT INTO Proveedor VALUES (0,?,NULL,?,?,?,?);",
        [
          req.body.ProvName,
          //Aqui falta la direccion
          req.body.ProvNumber,
          req.body.ProvWhats,
          req.body.ProvRFC,
          req.body.ProvEmail
        ],
        function(error, results, fields) {
          if (error) {
            console.error("Error while querying the database:  " + error);
            console.error(error.sql);
            res.json({
              status: "Something happened",
              Error: error
            });
            //throw error;
          }
          console.log("inserted " + results + " rows");
          console.log(results);
          res.json({
            status: "Success",
            msg: "inserted " + results.affectedRows + " rows",
            result: results
          });
        }
      );
    });

    server.post("/api/ConsultaPedido", upload.none(), (req, res) => {
      console.log(req.body);
      con.query(
        "SELECT * FROM Pedido WHERE id_pedido = ?;",
        [req.body.idPedido],
        function(error, results, fields) {
          if (error) {
            console.error("Error while querying the database:  " + error);
            console.error(error.sql);
            res.json({
              status: "Something happened",
              Error: error
            });
            //throw error;
          }
          console.log(results);
          if (results.length == 1) {
            res.json({
              status: "Success",
              result: results,
              msg: results[0].Nombre
            });
          } else {
            res.json({
              status: "NotSuccess",
              msg: "result lenght " + results.length
            });
            console.log("inserted " + results + " rows");
          }
        }
      );
    });
    server.post("/api/ConsultaEmpleado", upload.none(), (req, res) => {
      console.log(req.body);
      con.query(
        "SELECT * FROM Empleado WHERE id_empleado = ?;",
        [req.body.idEmpleado],
        function(error, results, fields) {
          if (error) {
            console.error("Error while querying the database:  " + error);
            console.error(error.sql);
            res.json({
              status: "Something happened",
              Error: error
            });
            //throw error;
          }
          console.log(results);
          if (results.length == 1) {
            res.json({
              status: "Success",
              result: results,
              msg: results[0].Nombre
            });
          } else {
            res.json({
              status: "NotSuccess",
              msg: "result lenght " + results.length
            });
            console.log("inserted " + results + " rows");
          }
        }
      );
    });
    server.post("/api/ConsultaCompra", upload.none(), (req, res) => {
      console.log(req.body);
      con.query(
        "SELECT * FROM Compra WHERE id_compra = ?;",
        [req.body.idCompra],
        function(error, results, fields) {
          if (error) {
            console.error("Error while querying the database:  " + error);
            console.error(error.sql);
            res.json({
              status: "Something happened",
              Error: error
            });
            //throw error;
          }
          console.log(results);
          if (results.length == 1) {
            res.json({
              status: "Success",
              result: results,
              msg: results[0].Nombre
            });
          } else {
            res.json({
              status: "NotSuccess",
              msg: "result lenght " + results.length
            });
            console.log("inserted " + results + " rows");
          }
        }
      );
    });
    server.post("/api/ConsultaProducto", upload.none(), (req, res) => {
      console.log(req.body);
      con.query(
        "SELECT * FROM Producto WHERE id_producto = ?;",
        [req.body.idProducto],
        function(error, results, fields) {
          if (error) {
            console.error("Error while querying the database:  " + error);
            console.error(error.sql);
            res.json({
              status: "Something happened",
              Error: error
            });
            //throw error;
          }
          console.log(results);
          if (results.length == 1) {
            res.json({
              status: "Success",
              result: results,
              msg: results[0].Nombre
            });
          } else {
            res.json({
              status: "NotSuccess",
              msg: "result lenght " + results.length
            });
            console.log("inserted " + results + " rows");
          }
        }
      );
    });
    server.post("/api/addProduct", upload.none(), (req, res) => {
      con.query(
        "INSERT INTO Producto VALUES (0,?,?,?,NULL,?,?,?,?);",
        [
          req.body.ProdDesc,
          req.body.ProdCost,
          req.body.ProdPrice,
          //Aqui falta categoria
          req.body.ProdExis,
          req.body.ProdReord,
          req.body.ProdUnit,
          req.body.ProdName
        ],
        function(error, results, fields) {
          if (error) {
            console.error("Error while querying the database:  " + error);
            console.error(error.sql);
            res.json({
              status: "Something happened",
              Error: error
            });
            //throw error;
          }
          console.log("inserted " + results.affectedRows + " rows");
          res.json({
            status: "Success",
            msg: "inserted " + results.affectedRows + " rows"
          });
        }
      );
    });

    server.post("/api/addEmployee", upload.none(), (req, res) => {
      con.query(
        "INSERT INTO Empleado VALUES (0,?,?,NULL,?,?,?,?);",
        [
          req.body.EmpName,
          req.body.EmpPuest,
          //Aqui falta id del jefe
          req.body.EmpTel,
          req.body.EmpEmail,
          req.body.EmpUsername,
          req.body.EmpPassword
        ],
        function(error, results, fields) {
          if (error) {
            console.error("Error while querying the database:  " + error);
            console.error(error.sql);
            res.json({
              status: "Something happened",
              Error: error
            });
            //throw error;
          }
          console.log("inserted " + results.affectedRows + " rows");
          res.json({
            status: "Success",
            msg: "inserted " + results.affectedRows + " rows"
          });
        }
      );
    });

    server.get("/ActualizarClienteEdit", upload.none(), (req, res) => {
      /*  const { key } = req.body;
    Do whatever you need to do
       const actualPage = "/";
      const queryParams = { key };
      console.log(req.query);  */
      return app.render(req, res, "/ActualizarClienteEdit", req.query);
      //res.json(req.body);
    });
    server.get("/c", (req, res) => {
      res.end(
        JSON.stringify({
          status: "ProductoActualizado"
        })
      );
      return res.end;
    });

    server.get("/posts/:id", (req, res) => {
      return app.render(req, res, "/posts", {
        id: req.params.id
      });
    });

    server.all("*", (req, res) => {
      return handle(req, res);
    });
    {
      /* server.use(session({
        secret: 'kerrisk'

    }));*/
    }
    server.listen(port, err => {
      if (err) throw err;
      console.log(`> Ready on http://localhost:${port}`);
    });
  })
  .catch(function(rej) {
    //here when you reject the promise
    console.log(rej);
  });

// apply the routes to our application
/* app.use('/', router);
app.use(express.static('public'));
var server = app.listen(8081, function () {
    var host = server.address().address
    var port = server.address().port
    console.log("Fruteria California listening at http://%s:%s", host, port);
}); */
