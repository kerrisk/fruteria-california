*DONE
• at least 8 tables
 at least 5 views
     • Mostrar todos los clientes activos (con una compra en el ultimo mes)
        CREATE OR REPLACE VIEW ClientesActivos AS (
            SELECT DISTINCT * FROM cliente WHERE cliente.id_Cliente in (
                SELECT DISTINCT id_cliente FROM pedido WHERE DATEDIFF(SYSDATE(), Fecha) < 31 
                )
        );
     • Mostrar todos los productos de la categoria frutas
        CREATE OR REPLACE VIEW Frutas AS (
            SELECT * FROM producto where id_productoCategoria in (
                SELECT id_productoCategoria FROM productocategoria WHERE Nombre = "Fruta"
            )
        );
     • Mostrar todos los productos de la categoria verduras
        CREATE OR REPLACE VIEW verduras AS (
            SELECT * FROM producto where id_productoCategoria in (
                SELECT id_productoCategoria FROM productocategoria WHERE Nombre = "Verdura"
            )
        );
     • Mostrar las compras que realizo un empleado filtrando con su nombre 
        CREATE OR REPLACE VIEW ComprasAlejandra AS (
            SELECT * FROM compra WHERE id_empleado in (
                SELECT id_empleado FROM empleado WHERE nombre = "Alejandra"
            )
        );
     • Mostrar los pedidos que realizo un empleado filtrando con su nombre  
         CREATE OR REPLACE VIEW PedidosAlejandra AS (
            SELECT * FROM pedido WHERE id_empleado in (
                SELECT id_empleado FROM empleado WHERE nombre = "Alejandra"
            )
        );
*TODO
** 

• at least 5 cursors
• at least 5 triggers
• several stored procedures
    -- Trigger al insertar en la tabla producto que calule el precio estimado de todo lo que 
    -- se tiente en la fruteria (multiplicando Existencia * CostoVenta)
    DROP PROCEDURE IF EXISTS p1;
    DROP TABLE IF EXISTS t1;
    CREATE TABLE t1 (existenciaxprecio float);

    DELIMITER //

    CREATE PROCEDURE p1()
    BEGIN
    DECLARE done INT DEFAULT FALSE;
    DECLARE Vexistencia float;
    DECLARE Vcostoventa float;
    DECLARE cur CURSOR FOR SELECT existencia, costoventa FROM producto;
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done=TRUE;
    OPEN cur;
    read_loop: LOOP
        FETCH cur INTO Vexistencia , Vcostoventa;
        IF done THEN
        LEAVE read_loop;
        END IF;
        INSERT INTO t1 VALUES (Vexistencia * Vcostoventa);
    END LOOP;
    CLOSE cur; 
    END;
    //
    DELIMITER ;

• at least 25 transactions
    • Cliente - 
        • Alta
        Insert into Cliente VALUES(0,req.body.registerName,req.body.registerEmail,req.body.registerPassword,req.body.registerNumber,NULL/*RFC*/);    
        • Baja
        DELETE FROM Cliente WHERE /*id de cliente a eliminar*/==id_cliente;
        • Cambio
    • Empleado - 
        • Alta
        INSERT INTO Empleado VALUES (0,req.body.EmpName,req.body.EmpPuest,req.body.idJefe/**/,req.body.EmpTel,req.body.EmpEmail,req.body.EmpUsername,req.body.EmpPassword);
        • Baja
        DELETE FROM Empleado WHERE /*id de empleado a eliminar*/==id_empleado;
        • Cambio
    • Compra
        • Alta
        INSERT INTO Compra VALUES (/*id de compra, id del proveedor, id del empleado, fecha, surtido, total*/)
        • Baja
        DELETE FROM Compra WHERE /*id de compra a eliminar*/==id_compra;
        •Cambio
    • Pedido
        • Alta
        INSERT INTO Pedido VALUES(/*id de pedido, id de cliente, id de empleado, Fecha, Estado, Surtido, Total*/);
        • Baja
        DELETE FROM Pedido WHERE /*id de pedido a eliminar*/==id_pedido;
        • Cambio
    • Proveedor
        • Alta
        INSERT INTO Proveedor VALUES (0,req.body.ProvName,NULL/*direccion*/,req.body.ProvNumber,req.body.ProvWhats,req.body.ProvRFC,req.body.ProvEmail);
        • Baja
        DELETE FROM Proveedor WHERE /*id de proveedor a eliminar*/==id_proveedor;
        • Cambio
    • Producto
        • Alta
        INSERT INTO Producto VALUES (0,req.body.ProdDesc,req.body.ProdCost,req.body.ProdPrice,NULL/*categoria*/,req.body.ProdExis,req.body.ProdReord,req.body.ProdUnit,req.body.ProdName);
        • Baja
        DELETE FROM Producto WHERE /*id de producto a eliminar*/==id_producto;
        • Cambio
    • ProductoCategoria
        • Alta
        INSERT INTO ProductoCategoria VALUES (/*Id de categoria, Descripcion, Nombre*/);
        • Baja
        DELETE FROM ProductoCategoria WHERE /*id de productoCategoria a eliminar*/==id_productoCategoria;
        • Cambio
    • ProveedorProducto
        • Alta
        INSERT INTO ProveedorProducto VALUES (/*Id de proveedor, Id de producto*/);
        • Baja
        DELETE FROM ProveedorProducto WHERE /*id de proveedor*/==id_proveedor AND /*id de producto*/==id_producto;
        • Cambio
    • ProductoImagen
        • Alta
        INSERT INTO ProductoImagen VALUES (/*Id de producto, ruta, id producto imagen*/);
        • Baja
        DELETE FROM ProductoImagen WHERE /*id de producto*/==id_producto;
        • Cambio
    • ClienteImagen
        • Alta
        INSERT INTO ClienteImagen VALUES (/*Id de cliente, ruta, id imagen del cliente*/);
        • Baja
        DELETE FROM ClienteImagen WHERE /*id de cliente*/==id_cliente;
        • Cambio
    • CompraProducto
        • Alta
        INSERT INTO CompraProducto VALUES (/*Id de producto, id compra,  cantidad, precio*/);
        • Baja
        DELETE FROM CompraProducto WHERE /*id de compra*/==id_compra AND /*id de producto*/==id_producto;
        • Cambio
    • PedidoProducto
        • Alta
        INSERT INTO PedidoProducto VALUES (/*Id de producto, id pedido,  cantidad, precio*/);
        • Baja
        DELETE FROM CompraProducto WHERE /*id de pedido*/==id_producto AND /*id de producto*/==id_producto;
        • Cambio
     10 of them must use at least 3 tables
     Usando 3 tablas 

    • Mostrar los productos de un pedido elegido por el cliente 
    /*Le mostramos al cliente todos los pedidos*/
    Select id_pedido,Fecha, Estado, Total FROM Pedido, Cliente WHERE Cliente.id_cliente==Pedido.id_cliente order by Pedido.Estado;
    /*Tomamos el id_pedido que el cliente quiera y desglozamos el el nombre y costo de cada producto del pedido*/
    Select Producto.Nombre, PedidoProducto.Precio, PedidoProducto.Cantidad FROM Pedido, Producto, PedidoProducto WHERE Pedido.id_pedido==PedidoProducto.id_pedido AND PedidoProducto.id_producto==Producto.id_producto;

    • Mostrar los productos que ha comprado un empleado, asi como cuanto a como y cuando lo compro
    /*Se toma el id del empleado de alguna variable o de la sesion*/
    SELECT Producto.Nombre, CompraProducto.Cantidad, CompraProducto.Precio, Compra.Fecha FROM Producto, CompraProducto, Compra WHERE id_empleado==Compra.id_empleado AND CompraProducto.id_compra==COMPRA.id_compra AND Producto.id_producto==CompraProducto.id_producto;

    • Mostrar la cantidad de dinero que se ha vendido de cada categoria 
    /*Tomamos la categoría de alguna variable*/
    SUM(SELECT PedidoProducto.Precio*PedidoProducto.Cantidad FROM ProductoCategoria,Producto, PedidoProducto WHERE ProductoCategoria.id_productoCategoria==Producto.id_productoCategoria AND Producto.id_producto==PedidoProducto.id_producto);

    • Mostrar imagen del cliente al empleado para cuando llegue lo atienda de manera corecta con su pedido
    /*El id del pedido será seleccionado anteriormente por el empleado*/
    Select ruta from Pedido, Cliente, ClienteImagen WHERE /*id_pedido seleccionado por el empleado*/==Pedido.id_pedido and Pedido.id_cliente=Cliente.id_cliente And Cliente.id_cliente==ClienteImagen.id_cliente;

    • Mostrar al empleado los productos de un pedido para checar si ya ha puesto todos lo productos
    /*Tomaremos el id del pedido que seleccionara anteriormente el empleado*/
    SELECT Producto.Nombre, PedidoProducto.cantidad FROM Producto, PedidoProducto, Pedido WHERE /*id_pedido elegido por el empleado*/==Pedido.id_pedido and Pedido.id_pedido==PedidoProducto.id_pedido and PedidoProducto.id_producto==Producto.id_producto GROUP BY Producto.nombre;

    • Mostrar los pedidos que han atendido cada empleado en el mes, con la finalidad de dar bonos extras al mejor
    SELECT Empleado.id_empleado, Empleado.nombre, count(*) as Pedidos From Empleado, Pedido WHERE Empleado.id_empleado==Pedido.id_empleado GROUP BY Empleado.id_empleado;

    • Mostrar el numero de compras hechas de cierto producto 
    /*Tomaremos el id del producto selecionado por el admin*/
    SELECT Producto.nombre, count(*) as Numero_Compras From Producto, CompraProducto, Compra WHERE Producto./*id_producto seleccionado*/==CompraProducto.id_producto and CompraProducto.id_compra==Compra.id_compra GROUP BY Producto.nombre;

    • Mostrar la cantidad de veces que ha sido atendido un cliente por un empleado
    /*Mostraremos una lista de forma descendente de las veces que un empleado ha atendido a un cliente utilizando parametros de cliente*/
    Select Empleado.nombre, count(*) as Numero FROM Empleado, Pedido, Cliente WHERE /*id_cliente*/==Pedido.id_cliente and Pedido.id_empleado==Empleado.id_empleado GROUP BY Numero DESC;

    • Mostrar los nombres de cada producto que nos ha vendido cierto proveedor
    /*Tomaremos el id del proveedor*/
    SELECT DISTINCT Producto.nombre FROM Producto, Proveedor, ProveedorProducto WHERE Proveedor.id_proveedor==ProveedorProducto.id_proveedor and ProveedorProducto.id_producto==Producto.id_producto;

    •Mostrar la cantidad que nos ha vendido los proveedores de cierto producto
    /*Tomaremos id del Producto y proveedor como parametros*/
    Select Proveedor.nombre, count(*) as Cantidad FROM Proveedor, Producto, ProveedorProducto, Compra, CompraProducto WHERE Producto.id_producto==ProveedorProducto.id_producto and ProveedorProducto.id_proveedor==Proveedor.id_proveedor and Proveedor.id_proveedor==Compra.id_proveedor and Compra.id_compra==CompraProducto.id_compra GROUP BY Proveedor.nombre;
    